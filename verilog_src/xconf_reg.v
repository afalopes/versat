`timescale 1ns / 1ps
`include "xdefs.v"
`include "xmem_map.v"

module xconf_reg(
		 input 			     clk,
		 input 			     rst,
		input 				     conf_req,
		input 				     conf_rw_rnw,
		 input [`INT_ADDR_W-1:0]     conf_addr,
		 input [`DATA_W-1:0]     conf_word,
		 input [`CONFIG_BITS-1:0]  conf_in,
		 input 			     conf_ld,
		 output [`CONFIG_BITS-1:0] conf_out
		 );

   reg [`CONFIG_BITS-1:0] 				      conf_reg;
   wire 			     conf_we;

   assign conf_out=conf_reg;
   assign conf_we=(conf_req & ~conf_rw_rnw)? 1'b1 : 1'b0;

   always @ (posedge clk, posedge rst) begin 
      if(rst) 
	conf_reg <= {`CONFIG_BITS{1'b0}};
      else if (conf_ld)
	conf_reg <= conf_in;
      else if (conf_we)
	case (conf_addr)
		
	  `CLEAR_CONFIG_ADDR:
	    conf_reg <= {`CONFIG_BITS{1'b0}};

	  `MEM0A_CONFIG_ADDR + `MEM_CONF_ITER_OFFSET:
	    conf_reg[`MEM0A_CONFIG_OFFSET-1 -: `ADDR_W] <= conf_word[`ADDR_W-1:0];
	  `MEM0A_CONFIG_ADDR + `MEM_CONF_PER_OFFSET:
	    conf_reg[`MEM0A_CONFIG_OFFSET-`ADDR_W-1 -: `PERIOD_W] <= conf_word[`PERIOD_W-1:0];
	  `MEM0A_CONFIG_ADDR + `MEM_CONF_DUTY_OFFSET:
	    conf_reg[`MEM0A_CONFIG_OFFSET-`ADDR_W-`PERIOD_W -1 -: `PERIOD_W] <= conf_word[`PERIOD_W-1:0];
	  `MEM0A_CONFIG_ADDR + `MEM_CONF_SELA_OFFSET:
	    conf_reg[`MEM0A_CONFIG_OFFSET-`ADDR_W-2*`PERIOD_W-1 -: `N_W] <= conf_word[`N_W-1:0];
	  `MEM0A_CONFIG_ADDR + `MEM_CONF_START_OFFSET:
	    conf_reg[`MEM0A_CONFIG_OFFSET-`ADDR_W-2*`PERIOD_W-`N_W-1 -: `ADDR_W] <= conf_word[`ADDR_W-1:0];
	  `MEM0A_CONFIG_ADDR + `MEM_CONF_INCR_OFFSET:
	    conf_reg[`MEM0A_CONFIG_OFFSET-2*`ADDR_W-2*`PERIOD_W-`N_W-1 -: `ADDR_W] <= conf_word[`ADDR_W-1:0];
	  `MEM0A_CONFIG_ADDR + `MEM_CONF_DELAY_OFFSET:
	    conf_reg[`MEM0A_CONFIG_OFFSET-3*`ADDR_W-2*`PERIOD_W-`N_W-1 -: `PERIOD_W] <= conf_word[`PERIOD_W-1:0];
	  `MEM0A_CONFIG_ADDR + `MEM_CONF_RVRS_OFFSET:
	    conf_reg[`MEM0A_CONFIG_OFFSET-3*`ADDR_W-3*`PERIOD_W-`N_W-1 -: 1] <= conf_word[0];


	  `MEM0B_CONFIG_ADDR + `MEM_CONF_ITER_OFFSET:
	    conf_reg[`MEM0B_CONFIG_OFFSET-1 -: `ADDR_W] <= conf_word[`ADDR_W-1:0];
	  `MEM0B_CONFIG_ADDR + `MEM_CONF_PER_OFFSET:
	    conf_reg[`MEM0B_CONFIG_OFFSET-`ADDR_W-1 -: `PERIOD_W] <= conf_word[`PERIOD_W-1:0];
	  `MEM0B_CONFIG_ADDR + `MEM_CONF_DUTY_OFFSET:
	    conf_reg[`MEM0B_CONFIG_OFFSET-`ADDR_W-`PERIOD_W -1 -: `PERIOD_W] <= conf_word[`PERIOD_W-1:0];
	  `MEM0B_CONFIG_ADDR + `MEM_CONF_SELA_OFFSET:
	    conf_reg[`MEM0B_CONFIG_OFFSET-`ADDR_W-2*`PERIOD_W-1 -: `N_W] <= conf_word[`N_W-1:0];
	  `MEM0B_CONFIG_ADDR + `MEM_CONF_START_OFFSET:
	    conf_reg[`MEM0B_CONFIG_OFFSET-`ADDR_W-2*`PERIOD_W-`N_W-1 -: `ADDR_W] <= conf_word[`ADDR_W-1:0];
	  `MEM0B_CONFIG_ADDR + `MEM_CONF_INCR_OFFSET:
	    conf_reg[`MEM0B_CONFIG_OFFSET-2*`ADDR_W-2*`PERIOD_W-`N_W-1 -: `ADDR_W] <= conf_word[`ADDR_W-1:0];
	  `MEM0B_CONFIG_ADDR + `MEM_CONF_DELAY_OFFSET:
	    conf_reg[`MEM0B_CONFIG_OFFSET-3*`ADDR_W-2*`PERIOD_W-`N_W-1 -: `PERIOD_W] <= conf_word[`PERIOD_W-1:0];
	  `MEM0B_CONFIG_ADDR + `MEM_CONF_RVRS_OFFSET:
	    conf_reg[`MEM0B_CONFIG_OFFSET-3*`ADDR_W-3*`PERIOD_W-`N_W-1 -: 1] <= conf_word[0];


	  `MEM1A_CONFIG_ADDR + `MEM_CONF_ITER_OFFSET:
	    conf_reg[`MEM1A_CONFIG_OFFSET-1 -: `ADDR_W] <= conf_word[`ADDR_W-1:0];
	  `MEM1A_CONFIG_ADDR + `MEM_CONF_PER_OFFSET:
	    conf_reg[`MEM1A_CONFIG_OFFSET-`ADDR_W-1 -: `PERIOD_W] <= conf_word[`PERIOD_W-1:0];
	  `MEM1A_CONFIG_ADDR + `MEM_CONF_DUTY_OFFSET:
	    conf_reg[`MEM1A_CONFIG_OFFSET-`ADDR_W-`PERIOD_W -1 -: `PERIOD_W] <= conf_word[`PERIOD_W-1:0];
	  `MEM1A_CONFIG_ADDR + `MEM_CONF_SELA_OFFSET:
	    conf_reg[`MEM1A_CONFIG_OFFSET-`ADDR_W-2*`PERIOD_W-1 -: `N_W] <= conf_word[`N_W-1:0];
	  `MEM1A_CONFIG_ADDR + `MEM_CONF_START_OFFSET:
	    conf_reg[`MEM1A_CONFIG_OFFSET-`ADDR_W-2*`PERIOD_W-`N_W-1 -: `ADDR_W] <= conf_word[`ADDR_W-1:0];
	  `MEM1A_CONFIG_ADDR + `MEM_CONF_INCR_OFFSET:
	    conf_reg[`MEM1A_CONFIG_OFFSET-2*`ADDR_W-2*`PERIOD_W-`N_W-1 -: `ADDR_W] <= conf_word[`ADDR_W-1:0];
	  `MEM1A_CONFIG_ADDR + `MEM_CONF_DELAY_OFFSET:
	    conf_reg[`MEM1A_CONFIG_OFFSET-3*`ADDR_W-2*`PERIOD_W-`N_W-1 -: `PERIOD_W] <= conf_word[`PERIOD_W-1:0];
	  `MEM1A_CONFIG_ADDR + `MEM_CONF_RVRS_OFFSET:
	    conf_reg[`MEM1A_CONFIG_OFFSET-3*`ADDR_W-3*`PERIOD_W-`N_W-1 -: 1] <= conf_word[0];


	  `MEM1B_CONFIG_ADDR + `MEM_CONF_ITER_OFFSET:
	    conf_reg[`MEM1B_CONFIG_OFFSET-1 -: `ADDR_W] <= conf_word[`ADDR_W-1:0];
	  `MEM1B_CONFIG_ADDR + `MEM_CONF_PER_OFFSET:
	    conf_reg[`MEM1B_CONFIG_OFFSET-`ADDR_W-1 -: `PERIOD_W] <= conf_word[`PERIOD_W-1:0];
	  `MEM1B_CONFIG_ADDR + `MEM_CONF_DUTY_OFFSET:
	    conf_reg[`MEM1B_CONFIG_OFFSET-`ADDR_W-`PERIOD_W -1 -: `PERIOD_W] <= conf_word[`PERIOD_W-1:0];
	  `MEM1B_CONFIG_ADDR + `MEM_CONF_SELA_OFFSET:
	    conf_reg[`MEM1B_CONFIG_OFFSET-`ADDR_W-2*`PERIOD_W-1 -: `N_W] <= conf_word[`N_W-1:0];
	  `MEM1B_CONFIG_ADDR + `MEM_CONF_START_OFFSET:
	    conf_reg[`MEM1B_CONFIG_OFFSET-`ADDR_W-2*`PERIOD_W-`N_W-1 -: `ADDR_W] <= conf_word[`ADDR_W-1:0];
	  `MEM1B_CONFIG_ADDR + `MEM_CONF_INCR_OFFSET:
	    conf_reg[`MEM1B_CONFIG_OFFSET-2*`ADDR_W-2*`PERIOD_W-`N_W-1 -: `ADDR_W] <= conf_word[`ADDR_W-1:0];
	  `MEM1B_CONFIG_ADDR + `MEM_CONF_DELAY_OFFSET:
	    conf_reg[`MEM1B_CONFIG_OFFSET-3*`ADDR_W-2*`PERIOD_W-`N_W-1 -: `PERIOD_W] <= conf_word[`PERIOD_W-1:0];
	  `MEM1B_CONFIG_ADDR + `MEM_CONF_RVRS_OFFSET:
	    conf_reg[`MEM1B_CONFIG_OFFSET-3*`ADDR_W-3*`PERIOD_W-`N_W-1 -: 1] <= conf_word[0];


	  `MEM2A_CONFIG_ADDR + `MEM_CONF_ITER_OFFSET:
	    conf_reg[`MEM2A_CONFIG_OFFSET-1 -: `ADDR_W] <= conf_word[`ADDR_W-1:0];
	  `MEM2A_CONFIG_ADDR + `MEM_CONF_PER_OFFSET:
	    conf_reg[`MEM2A_CONFIG_OFFSET-`ADDR_W-1 -: `PERIOD_W] <= conf_word[`PERIOD_W-1:0];
	  `MEM2A_CONFIG_ADDR + `MEM_CONF_DUTY_OFFSET:
	    conf_reg[`MEM2A_CONFIG_OFFSET-`ADDR_W-`PERIOD_W -1 -: `PERIOD_W] <= conf_word[`PERIOD_W-1:0];
	  `MEM2A_CONFIG_ADDR + `MEM_CONF_SELA_OFFSET:
	    conf_reg[`MEM2A_CONFIG_OFFSET-`ADDR_W-2*`PERIOD_W-1 -: `N_W] <= conf_word[`N_W-1:0];
	  `MEM2A_CONFIG_ADDR + `MEM_CONF_START_OFFSET:
	    conf_reg[`MEM2A_CONFIG_OFFSET-`ADDR_W-2*`PERIOD_W-`N_W-1 -: `ADDR_W] <= conf_word[`ADDR_W-1:0];
	  `MEM2A_CONFIG_ADDR + `MEM_CONF_INCR_OFFSET:
	    conf_reg[`MEM2A_CONFIG_OFFSET-2*`ADDR_W-2*`PERIOD_W-`N_W-1 -: `ADDR_W] <= conf_word[`ADDR_W-1:0];
	  `MEM2A_CONFIG_ADDR + `MEM_CONF_DELAY_OFFSET:
	    conf_reg[`MEM2A_CONFIG_OFFSET-3*`ADDR_W-2*`PERIOD_W-`N_W-1 -: `PERIOD_W] <= conf_word[`PERIOD_W-1:0];
	  `MEM2A_CONFIG_ADDR + `MEM_CONF_RVRS_OFFSET:
	    conf_reg[`MEM2A_CONFIG_OFFSET-3*`ADDR_W-3*`PERIOD_W-`N_W-1 -: 1] <= conf_word[0];


	  `MEM2B_CONFIG_ADDR + `MEM_CONF_ITER_OFFSET:
	    conf_reg[`MEM2B_CONFIG_OFFSET-1 -: `ADDR_W] <= conf_word[`ADDR_W-1:0];
	  `MEM2B_CONFIG_ADDR + `MEM_CONF_PER_OFFSET:
	    conf_reg[`MEM2B_CONFIG_OFFSET-`ADDR_W-1 -: `PERIOD_W] <= conf_word[`PERIOD_W-1:0];
	  `MEM2B_CONFIG_ADDR + `MEM_CONF_DUTY_OFFSET:
	    conf_reg[`MEM2B_CONFIG_OFFSET-`ADDR_W-`PERIOD_W -1 -: `PERIOD_W] <= conf_word[`PERIOD_W-1:0];
	  `MEM2B_CONFIG_ADDR + `MEM_CONF_SELA_OFFSET:
	    conf_reg[`MEM2B_CONFIG_OFFSET-`ADDR_W-2*`PERIOD_W-1 -: `N_W] <= conf_word[`N_W-1:0];
	  `MEM2B_CONFIG_ADDR + `MEM_CONF_START_OFFSET:
	    conf_reg[`MEM2B_CONFIG_OFFSET-`ADDR_W-2*`PERIOD_W-`N_W-1 -: `ADDR_W] <= conf_word[`ADDR_W-1:0];
	  `MEM2B_CONFIG_ADDR + `MEM_CONF_INCR_OFFSET:
	    conf_reg[`MEM2B_CONFIG_OFFSET-2*`ADDR_W-2*`PERIOD_W-`N_W-1 -: `ADDR_W] <= conf_word[`ADDR_W-1:0];
	  `MEM2B_CONFIG_ADDR + `MEM_CONF_DELAY_OFFSET:
	    conf_reg[`MEM2B_CONFIG_OFFSET-3*`ADDR_W-2*`PERIOD_W-`N_W-1 -: `PERIOD_W] <= conf_word[`PERIOD_W-1:0];
	  `MEM2B_CONFIG_ADDR + `MEM_CONF_RVRS_OFFSET:
	    conf_reg[`MEM2B_CONFIG_OFFSET-3*`ADDR_W-3*`PERIOD_W-`N_W-1 -: 1] <= conf_word[0];


	  `MEM3A_CONFIG_ADDR + `MEM_CONF_ITER_OFFSET:
	    conf_reg[`MEM3A_CONFIG_OFFSET-1 -: `ADDR_W] <= conf_word[`ADDR_W-1:0];
	  `MEM3A_CONFIG_ADDR + `MEM_CONF_PER_OFFSET:
	    conf_reg[`MEM3A_CONFIG_OFFSET-`ADDR_W-1 -: `PERIOD_W] <= conf_word[`PERIOD_W-1:0];
	  `MEM3A_CONFIG_ADDR + `MEM_CONF_DUTY_OFFSET:
	    conf_reg[`MEM3A_CONFIG_OFFSET-`ADDR_W-`PERIOD_W -1 -: `PERIOD_W] <= conf_word[`PERIOD_W-1:0];
	  `MEM3A_CONFIG_ADDR + `MEM_CONF_SELA_OFFSET:
	    conf_reg[`MEM3A_CONFIG_OFFSET-`ADDR_W-2*`PERIOD_W-1 -: `N_W] <= conf_word[`N_W-1:0];
	  `MEM3A_CONFIG_ADDR + `MEM_CONF_START_OFFSET:
	    conf_reg[`MEM3A_CONFIG_OFFSET-`ADDR_W-2*`PERIOD_W-`N_W-1 -: `ADDR_W] <= conf_word[`ADDR_W-1:0];
	  `MEM3A_CONFIG_ADDR + `MEM_CONF_INCR_OFFSET:
	    conf_reg[`MEM3A_CONFIG_OFFSET-2*`ADDR_W-2*`PERIOD_W-`N_W-1 -: `ADDR_W] <= conf_word[`ADDR_W-1:0];
	  `MEM3A_CONFIG_ADDR + `MEM_CONF_DELAY_OFFSET:
	    conf_reg[`MEM3A_CONFIG_OFFSET-3*`ADDR_W-2*`PERIOD_W-`N_W-1 -: `PERIOD_W] <= conf_word[`PERIOD_W-1:0];
	  `MEM3A_CONFIG_ADDR + `MEM_CONF_RVRS_OFFSET:
	    conf_reg[`MEM3A_CONFIG_OFFSET-3*`ADDR_W-3*`PERIOD_W-`N_W-1 -: 1] <= conf_word[0];


	  `MEM3B_CONFIG_ADDR + `MEM_CONF_ITER_OFFSET:
	    conf_reg[`MEM3B_CONFIG_OFFSET-1 -: `ADDR_W] <= conf_word[`ADDR_W-1:0];
	  `MEM3B_CONFIG_ADDR + `MEM_CONF_PER_OFFSET:
	    conf_reg[`MEM3B_CONFIG_OFFSET-`ADDR_W-1 -: `PERIOD_W] <= conf_word[`PERIOD_W-1:0];
	  `MEM3B_CONFIG_ADDR + `MEM_CONF_DUTY_OFFSET:
	    conf_reg[`MEM3B_CONFIG_OFFSET-`ADDR_W-`PERIOD_W -1 -: `PERIOD_W] <= conf_word[`PERIOD_W-1:0];
	  `MEM3B_CONFIG_ADDR + `MEM_CONF_SELA_OFFSET:
	    conf_reg[`MEM3B_CONFIG_OFFSET-`ADDR_W-2*`PERIOD_W-1 -: `N_W] <= conf_word[`N_W-1:0];
	  `MEM3B_CONFIG_ADDR + `MEM_CONF_START_OFFSET:
	    conf_reg[`MEM3B_CONFIG_OFFSET-`ADDR_W-2*`PERIOD_W-`N_W-1 -: `ADDR_W] <= conf_word[`ADDR_W-1:0];
	  `MEM3B_CONFIG_ADDR + `MEM_CONF_INCR_OFFSET:
	    conf_reg[`MEM3B_CONFIG_OFFSET-2*`ADDR_W-2*`PERIOD_W-`N_W-1 -: `ADDR_W] <= conf_word[`ADDR_W-1:0];
	  `MEM3B_CONFIG_ADDR + `MEM_CONF_DELAY_OFFSET:
	    conf_reg[`MEM3B_CONFIG_OFFSET-3*`ADDR_W-2*`PERIOD_W-`N_W-1 -: `PERIOD_W] <= conf_word[`PERIOD_W-1:0];
	  `MEM3B_CONFIG_ADDR + `MEM_CONF_RVRS_OFFSET:
	    conf_reg[`MEM3B_CONFIG_OFFSET-3*`ADDR_W-3*`PERIOD_W-`N_W-1 -: 1] <= conf_word[0];


	  `ALU0_CONFIG_ADDR + `ALU_CONF_FNS_OFFSET:
	    conf_reg[`ALU0_CONFIG_OFFSET-1 -: `ALU_FNS_W] <= conf_word[`ALU_FNS_W-1:0];
	  `ALU0_CONFIG_ADDR + `ALU_CONF_SELA_OFFSET:
	    conf_reg[`ALU0_CONFIG_OFFSET-`ALU_FNS_W-1 -: `N_W] <= conf_word[`N_W-1:0];
	  `ALU0_CONFIG_ADDR + `ALU_CONF_SELB_OFFSET:
	    conf_reg[`ALU0_CONFIG_OFFSET-`ALU_FNS_W-`N_W-1 -: `N_W] <= conf_word[`N_W-1:0];


	  `ALU1_CONFIG_ADDR + `ALU_CONF_FNS_OFFSET:
	    conf_reg[`ALU1_CONFIG_OFFSET-1 -: `ALU_FNS_W] <= conf_word[`ALU_FNS_W-1:0];
	  `ALU1_CONFIG_ADDR + `ALU_CONF_SELA_OFFSET:
	    conf_reg[`ALU1_CONFIG_OFFSET-`ALU_FNS_W-1 -: `N_W] <= conf_word[`N_W-1:0];
	  `ALU1_CONFIG_ADDR + `ALU_CONF_SELB_OFFSET:
	    conf_reg[`ALU1_CONFIG_OFFSET-`ALU_FNS_W-`N_W-1 -: `N_W] <= conf_word[`N_W-1:0];


	  `ALU2_CONFIG_ADDR + `ALU_CONF_FNS_OFFSET:
	    conf_reg[`ALU2_CONFIG_OFFSET-1 -: `ALU_FNS_W] <= conf_word[`ALU_FNS_W-1:0];
	  `ALU2_CONFIG_ADDR + `ALU_CONF_SELA_OFFSET:
	    conf_reg[`ALU2_CONFIG_OFFSET-`ALU_FNS_W-1 -: `N_W] <= conf_word[`N_W-1:0];
	  `ALU2_CONFIG_ADDR + `ALU_CONF_SELB_OFFSET:
	    conf_reg[`ALU2_CONFIG_OFFSET-`ALU_FNS_W-`N_W-1 -: `N_W] <= conf_word[`N_W-1:0];


	  `ALU3_CONFIG_ADDR + `ALU_CONF_FNS_OFFSET:
	    conf_reg[`ALU3_CONFIG_OFFSET-1 -: `ALU_FNS_W] <= conf_word[`ALU_FNS_W-1:0];
	  `ALU3_CONFIG_ADDR + `ALU_CONF_SELA_OFFSET:
	    conf_reg[`ALU3_CONFIG_OFFSET-`ALU_FNS_W-1 -: `N_W] <= conf_word[`N_W-1:0];
	  `ALU3_CONFIG_ADDR + `ALU_CONF_SELB_OFFSET:
	    conf_reg[`ALU3_CONFIG_OFFSET-`ALU_FNS_W-`N_W-1 -: `N_W] <= conf_word[`N_W-1:0];


	  `MULT0_CONFIG_ADDR + `MUL_CONF_SELA_OFFSET:
	    conf_reg[`MULT0_CONFIG_OFFSET-1 -: `N_W] <= conf_word[`N_W-1:0];
	  `MULT0_CONFIG_ADDR + `MUL_CONF_SELB_OFFSET:
	    conf_reg[`MULT0_CONFIG_OFFSET-`N_W-1 -: `N_W] <= conf_word[`N_W-1:0];
	  `MULT0_CONFIG_ADDR + `MUL_CONF_LONHI_OFFSET:
	    conf_reg[`MULT0_CONFIG_OFFSET-2*`N_W-1 -: 1] <= conf_word[0];
	  `MULT0_CONFIG_ADDR + `MUL_CONF_DIV2_OFFSET:
	    conf_reg[`MULT0_CONFIG_OFFSET-2*`N_W-2 -: 1] <= conf_word[0];


	  `MULT1_CONFIG_ADDR + `MUL_CONF_SELA_OFFSET:
	    conf_reg[`MULT1_CONFIG_OFFSET-1 -: `N_W] <= conf_word[`N_W-1:0];
	  `MULT1_CONFIG_ADDR + `MUL_CONF_SELB_OFFSET:
	    conf_reg[`MULT1_CONFIG_OFFSET-`N_W-1 -: `N_W] <= conf_word[`N_W-1:0];
	  `MULT1_CONFIG_ADDR + `MUL_CONF_LONHI_OFFSET:
	    conf_reg[`MULT1_CONFIG_OFFSET-2*`N_W-1 -: 1] <= conf_word[0];
	  `MULT1_CONFIG_ADDR + `MUL_CONF_DIV2_OFFSET:
	    conf_reg[`MULT1_CONFIG_OFFSET-2*`N_W-2 -: 1] <= conf_word[0];


	  `MULT2_CONFIG_ADDR + `MUL_CONF_SELA_OFFSET:
	    conf_reg[`MULT2_CONFIG_OFFSET-1 -: `N_W] <= conf_word[`N_W-1:0];
	  `MULT2_CONFIG_ADDR + `MUL_CONF_SELB_OFFSET:
	    conf_reg[`MULT2_CONFIG_OFFSET-`N_W-1 -: `N_W] <= conf_word[`N_W-1:0];
	  `MULT2_CONFIG_ADDR + `MUL_CONF_LONHI_OFFSET:
	    conf_reg[`MULT2_CONFIG_OFFSET-2*`N_W-1 -: 1] <= conf_word[0];
	  `MULT2_CONFIG_ADDR + `MUL_CONF_DIV2_OFFSET:
	    conf_reg[`MULT2_CONFIG_OFFSET-2*`N_W-2 -: 1] <= conf_word[0];


	  `MULT3_CONFIG_ADDR + `MUL_CONF_SELA_OFFSET:
	    conf_reg[`MULT3_CONFIG_OFFSET-1 -: `N_W] <= conf_word[`N_W-1:0];
	  `MULT3_CONFIG_ADDR + `MUL_CONF_SELB_OFFSET:
	    conf_reg[`MULT3_CONFIG_OFFSET-`N_W-1 -: `N_W] <= conf_word[`N_W-1:0];
	  `MULT3_CONFIG_ADDR + `MUL_CONF_LONHI_OFFSET:
	    conf_reg[`MULT3_CONFIG_OFFSET-2*`N_W-1 -: 1] <= conf_word[0];
	  `MULT3_CONFIG_ADDR + `MUL_CONF_DIV2_OFFSET:
	    conf_reg[`MULT3_CONFIG_OFFSET-2*`N_W-2 -: 1] <= conf_word[0];


          (`BS0_CONFIG_ADDR + `BS_CONF_SELA_OFFSET):
            conf_reg[`BS0_CONFIG_OFFSET-1 -:`N_W] <= conf_word[`N_W-1:0];		
          `BS0_CONFIG_ADDR + `BS_CONF_SELB_OFFSET:
            conf_reg[`BS0_CONFIG_OFFSET-`N_W-1 -:`N_W] <= conf_word[`N_W-1:0];
          `BS0_CONFIG_ADDR + `BS_CONF_LNA_OFFSET:
            conf_reg[`BS0_CONFIG_OFFSET-2*`N_W-1 -: 1] <= conf_word[0];
          `BS0_CONFIG_ADDR + `BS_CONF_LNR_OFFSET:
            conf_reg[`BS0_CONFIG_OFFSET-2*`N_W-2 -: 1] <= conf_word[0];


          `BS1_CONFIG_ADDR + `BS_CONF_SELA_OFFSET:
            conf_reg[`BS1_CONFIG_OFFSET-1 -:`N_W] <= conf_word[`N_W-1:0];
          `BS1_CONFIG_ADDR + `BS_CONF_SELB_OFFSET:
            conf_reg[`BS1_CONFIG_OFFSET-`N_W-1 -:`N_W] <= conf_word[`N_W-1:0];
          `BS1_CONFIG_ADDR + `BS_CONF_LNA_OFFSET:
            conf_reg[`BS1_CONFIG_OFFSET-2*`N_W-1 -: 1] <= conf_word[0];
          `BS1_CONFIG_ADDR + `BS_CONF_LNR_OFFSET:
            conf_reg[`BS1_CONFIG_OFFSET-2*`N_W-2 -: 1] <= conf_word[0];

	endcase
     end

endmodule
