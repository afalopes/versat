/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: Testbench wb_bfm_memory

   Copyright (C) 2014 Authors

  Author(s): Guilherme Luz <gui_luz_93@hotmail.com>

***************************************************************************** */

`timescale 1ns / 1ps

module ram_wb_b3_tb;
 
   parameter dw = 32;
   parameter aw = 32;

   // Memory parameters
   parameter memory_file = "";

   parameter clk_per = 10;

   //input
   reg wb_clk_i;
   reg wb_rst_i;
   
   reg [aw-1:0] wb_adr_i;
   reg [dw-1:0] wb_dat_i;
   reg [3:0] wb_sel_i;
   reg wb_we_i;
   reg [1:0] wb_bte_i;
   reg [2:0] wb_cti_i;
   reg wb_cyc_i;
   reg wb_stb_i;
   
   //output
   wire wb_ack_o;
   wire wb_err_o;
   wire wb_rty_o;
   wire [dw-1:0] wb_dat_o;

   ram_wb_b3 uut (
                 .wb_clk_i(wb_clk_i),
                 .wb_rst_i(wb_rst_i), 
                 .wb_adr_i(wb_adr_i),
                 .wb_dat_i(wb_dat_i),
                 .wb_sel_i(wb_sel_i),
                 .wb_we_i(wb_we_i),
                 .wb_bte_i(wb_bte_i),
                 .wb_cti_i(wb_cti_i),
                 .wb_cyc_i(wb_cyc_i),
                 .wb_stb_i(wb_stb_i),
                 .wb_ack_o(wb_ack_o),
                 .wb_err_o(wb_err_o),
                 .wb_rty_o(wb_rty_o),
                 .wb_dat_o(wb_dat_o)
   );


   initial begin
      $dumpfile("wb_bfm_memory.vcd");
      $dumpvars();  


      // Initialize Inputs
      wb_rst_i=1;
      wb_clk_i=0;
      wb_we_i=0;
      wb_sel_i=4'b1111;
      wb_cyc_i=0;
      wb_stb_i=0;
      wb_bte_i=2'b01;
      wb_cti_i=3'b010;

      //Add stimulus here
      //Classic standard SINGLE WRITE Cycle
      #(clk_per*2-4)
	  wb_rst_i=0;
      wb_adr_i={14'd100,2'b00};
      wb_dat_i=3;
      wb_we_i=1;	
      wb_cyc_i=1;
      wb_stb_i=1;
      
	  #clk_per

      wb_adr_i={14'd100,2'b00};
      wb_dat_i=4;
    
      #(clk_per-1)       
      wb_adr_i={14'd101,2'b00};
      wb_dat_i=5;

      #clk_per
      wb_dat_i=6;
      wb_adr_i={14'd102,2'b00};
      
	  #clk_per
      wb_adr_i={14'd103,2'b00};
      wb_dat_i=4;
      wb_dat_i=7;     
      
     
	  #(1)
      wb_dat_i=8;
      wb_cti_i=3'b111;

      //Classic standard SINGLE READ Cycle
       #(clk_per)
	   wb_we_i=0;
	   wb_stb_i=0;
       wb_cti_i=3'b010;
	   #clk_per
       
       #(clk_per+1)
       wb_adr_i={14'd101,2'b00};
       wb_cyc_i=1;
       wb_stb_i=1;
	   
	   #clk_per
       wb_adr_i={14'd101,2'b00};
   
       #(clk_per-2)       
       wb_adr_i={14'd102,2'b00};
       wb_dat_i=5;

       #clk_per
       wb_dat_i=6;
       wb_adr_i={14'd103,2'b00};
      
	   #clk_per
       wb_adr_i={14'd100,2'b00};
       wb_dat_i=4;
       wb_dat_i=7;     
       #1
	   wb_cti_i=3'b111;
       #(clk_per-1)
		wb_cyc_i=0;
	    wb_stb_i=0;
	   


       #40 $finish;
   end
	
   always
       #(clk_per/2)  wb_clk_i =  ~ wb_clk_i;

endmodule 
   

