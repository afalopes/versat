/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
            
***************************************************************************** */
`timescale 1ns / 1ps
`include "./xdefs.v"


module xinstr_mem(

	 //control 
	 input clk,

	 //controller interface
	 input  [`ADDR_W-1:0]     pc,
	 output reg [`DATA_W-1:0] instruction,
	 	 
	 //dma interface
 	 input 	                 dma_wnr,
     input [`ADDR_W-1:0]     dma_addr,
     input [`DATA_W-1:0] 	 dma_data

  );
   reg  [`DATA_W-1:0]     mem[2**`ADDR_W-1:0];
   wire [`ADDR_W-1:0] 	  addr;
   assign addr=(dma_wnr)? dma_addr : pc;

   always @(posedge clk) begin			
         if (dma_wnr)
            mem[addr] <= dma_data;
         instruction <= mem[addr];
   end

endmodule
