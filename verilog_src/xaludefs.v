/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>

***************************************************************************** */



// ALU functions
`define ALU_LOGIC_OR      5'd0
`define ALU_LOGIC_AND     5'd1 
`define ALU_LOGIC_ANDN    5'd2
`define ALU_LOGIC_XOR     5'd3 
`define ALU_SEXT8         5'd4 
`define ALU_SEXT16        5'd5 
`define ALU_SHIFTR_ARTH   5'd6 
`define ALU_SHIFTR_LOG    5'd7 
`define ALU_CMP_UNS       5'd13      
`define ALU_CMP_SIG   	  5'd14      
`define ALU_ADD           5'd16 
`define ALU_SUB           5'd17

//PATTERN COMPARE functions 
`define ALU_PCBF          5'd9
`define ALU_PCE           5'd10
`define ALU_PCNE          5'd11
`define ALU_PCCLZ         5'd12

//EXT2 functions
`define ALU_MAX       	  5'd18 		
`define ALU_MIN       	  5'd19 
`define ALU_ABS        	  5'd20
