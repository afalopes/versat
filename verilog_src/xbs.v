/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
             Joao Dias Lopes <joao.d.lopes91@gmail.com>

***************************************************************************** */

`timescale 1ns / 1ps
`include "xdefs.v"

module xbs(

	//control 
	input clk,
        input rst,
	
        //controller interface
        input rw_req,
	input rw_rnw,
	input [`DATA_W-1:0] rw_data_to_wr,
	
	//data 
	input [`N*`DATA_W-1:0] data_in_bus,
   output reg signed [`DATA_W-1:0] data_out,
	 
	//config data
	input [`BS_CONFIG_BITS-1:0] configdata

 );


   wire signed [`DATA_W-1:0] data_in;
   wire	[`DATA_W-1:0] shift;
	
	reg [`DATA_W-1:0] res;

	//wiring config data
	wire [`N_W-1: 0] seldata;
	wire [`N_W-1: 0] selshift;
   wire log_narith;
   wire left_nright;

	wire enabledA, enabledB;

	//unpack config bits
	assign seldata = configdata[`BS_CONFIG_BITS-1 -: `N_W];
	assign selshift = configdata[`BS_CONFIG_BITS-`N_W-1 -: `N_W];
	assign log_narith = configdata[1];
	assign left_nright = configdata[0];
	

	//input selection 
	xinmux muxdata (
		.sel(seldata),
		.data_in_bus(data_in_bus),
		.data_out(data_in),
		.enabled(enabledA)
	);
	
	xinmux muxshift (
		.sel(selshift),
		.data_in_bus(data_in_bus),
		.data_out(shift),
		.enabled(enabledB)
	);

	always @ (*) begin 
		if(left_nright)
			res = data_in << shift[4:0]; 
		else 
			if (log_narith)
				res = data_in >> shift[4:0];
			else	
				res = data_in >>> shift[4:0];
	end

	always @ (posedge clk) 
		if (rst)
		  data_out <= `DATA_W'h00000000;
		else begin
	           if(rw_req & ~rw_rnw) 
		     data_out <= rw_data_to_wr;
		   else if(enabledA & enabledB)
		     data_out <= res;
 		end
endmodule
