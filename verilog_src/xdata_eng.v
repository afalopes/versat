/* ****************************************************************************
 This Source Code Form is subject to the terms of the
 Open Hardware Description License, v. 1.0. If a copy
 of the OHDL was not distributed with this file, You
 can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

 Description: 

 Copyright (C) 2014 Authors

 Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
            Joao Dias Lopes <joao.d.lopes91@gmail.com>
            Andre Lopes <andre.a.lopes@netcabo.pt>

  ***************************************************************************** */

`timescale 1ns / 1ps

  /* ENGINE STATUS REG: 

   BIT 0: busy


   ENGINE CONTROL REG: 

   BIT 0: INIT
   BIT 1: RUN

   ENGINE STATES 

   IDLE:  if INIT command stays IDLE. if RUN command goes to BUSY.
   BUSY: if receives done signal goes to IDLE.

   */

`include "xdefs.v"
`include "xmem_map.v"

  module xdata_eng (
		    input 		     clk,
		    input 		     en,

		    input [`CONFIG_BITS-1:0] config_bus,
		    
		    //controller interface
		    input 		     rw_req,
		    input 		     rw_rnw,
		    input [`INT_ADDR_W-1:0]  rw_addr,
		    input [`DATA_W-1:0]      rw_data_to_wr,
		    output reg [`DATA_W-1:0] rw_data_to_rd,
  
		    //dma interface
 		    input 		     dma_req,
 		    input 		     dma_rnw,
 		    input [`INT_ADDR_W-2:0]  dma_addr,
		    input [`DATA_W-1:0]      dma_data_in,
	            output reg [`DATA_W-1:0] dma_data_out,
		    output 		     mem_rdy
			);
   
   wire [2*`Nmem-1:0] 			     mem_done;
   wire 				     run_bit, init_bit;
   
   wire 				     ctrl_req;
   wire 				     status_req;
   wire [`DATA_W-1: 0] 			     status;
   
   wire [`Nmem-1:0] 			     dma_mem_req;
   wire [`Nmem-1:0] 			     mem_req;	
   wire [`N-4:0] 			     fu_regf_req;
   //all mems and fus contribute their outputs to this bus
   wire [`DATA_BITS-1:0] 		     data_bus;
   
   //state machine
   reg 					     state, state_nxt;
   parameter IDLE=1'b0, BUSY=1'b1;
   
   wire [`Nalu-1:0] 			     carry;
   
   wire 				     eng_en;

   reg [`CONFIG_BITS-1:0] 		     config_reg_shadow;
   
   //
   // description
   //

   
   assign ctrl_req = (rw_addr == `ENG_CTRL_REG)? rw_req : 1'b0;
   assign status_req = (rw_addr == `ENG_STATUS_REG)? rw_req : 1'b0;
   
   assign status = {{(`DATA_W-12){1'b0}}, carry, mem_done};

   assign init_bit = ctrl_req & rw_data_to_wr[0];
   assign run_bit = ctrl_req & rw_data_to_wr[1];
   assign eng_en = state_nxt;
   
   //assign special data bus entries: 0, 1 and rw_data_to_wr
   assign data_bus[`DATA_BITS-1 -: `DATA_W] = rw_data_to_wr;
   assign data_bus[`DATA_BITS-1-`DATA_W -: `DATA_W] = `DATA_W'd0; //zero constant
   assign data_bus[`DATA_BITS-1-2*`DATA_W -: `DATA_W] = `DATA_W'd1;
   
   assign mem_rdy = mem_done[dma_addr[12:11]];
   genvar 				     i;

   always @ (*) begin
	   case (dma_addr[12:11])
		0 : dma_data_out=data_bus[`DATA_BITS - (`smem0A+2*0)*`DATA_W-1 -: `DATA_W];
		1 : dma_data_out=data_bus[`DATA_BITS - (`smem0A+2*1)*`DATA_W-1 -: `DATA_W];
		2 : dma_data_out=data_bus[`DATA_BITS - (`smem0A+2*2)*`DATA_W-1 -: `DATA_W];
		3 : dma_data_out=data_bus[`DATA_BITS - (`smem0A+2*3)*`DATA_W-1 -: `DATA_W];
	   endcase
   end

   always @ (posedge clk) begin
      if(init_bit)
	config_reg_shadow <= config_bus;
   end

   //state machine
   always @ (*) begin 
      state_nxt = state;
      if (state == IDLE) begin
	 if (run_bit) 
	   state_nxt = BUSY;
      end else begin//state == BUSY
	 if(init_bit)  
	   state_nxt = IDLE;
      end 		
   end 

   //select read data
   always @ (*) begin : data_read_sel
      integer j;
      rw_data_to_rd = status;

      if(~status_req) begin
	 for (j=0; j < `Nmem; j= j+1)
	   if (mem_req[j])
	     rw_data_to_rd = data_bus[`DATA_BITS - (`smem0B+2*j)*`DATA_W -: `DATA_W];	
	 for (j=0; j < `N; j= j+1)
	   if (fu_regf_req[j])
	     rw_data_to_rd = data_bus[`DATA_BITS - j*`DATA_W -1 -: `DATA_W];				
      end 
   end 
   
   
   xeng_addr_decoder eng_addr_decoder(
				      .rw_req(rw_req),
				      .rw_addr(rw_addr),
				      .ctrl_req(ctrl_req),
				      .status_req(status_req),
				      .fu_regf_req(fu_regf_req),
				      .mem_req(mem_req),
					
				      .dma_req(dma_req),
				      .dma_addr(dma_addr),
				      .dma_mem_req(dma_mem_req)

				      );

   // Instantiate the data memories
   generate
      for (i=0; i < `Nmem; i=i+1) begin : mem_array
	 xmem mem (
		   .clk(clk),
		   .rst((init_bit&rw_data_to_wr[`mem_idx-i-1])),
		   .en(eng_en),
		   .done(mem_done[2*i+1:2*i]),

		   //controller interface
		   .rw_addrA_req(fu_regf_req[`smem0A+2*i-4]),
		   .rw_addrB_req(fu_regf_req[`smem0B+2*i-4]),
		   .rw_mem_req(mem_req[i]),
		   .rw_rnw(rw_rnw),
		   .rw_addr(rw_addr[`ADDR_W-1:0]),
		   .rw_data_to_wr(rw_data_to_wr),
 		   
		   //dma interface
		   .dma_addr(dma_addr[`ADDR_W-1:0]),
		   .dma_rnw(dma_rnw),
		   .dma_mem_req(dma_mem_req[i]),
		   
		    // data io
		   .dma_data_in(dma_data_in),
           .data_in_bus(data_bus),
		   .outA(data_bus[`DATA_BITS - (`smem0A+2*i-1)*`DATA_W-1 -: `DATA_W]),
		   .outB(data_bus[`DATA_BITS - (`smem0B+2*i-1)*`DATA_W-1 -: `DATA_W]),
		   
		   //configuration data 
		   .config_bits(config_reg_shadow[`MEM0A_CONFIG_OFFSET-2*i*`MEMP_CONFIG_BITS-1 -: 2*`MEMP_CONFIG_BITS]) //it needs multiply 2 times (MEMP_CONFIG_BITS) because each memory has 2 ports (A and B)
		   );
      end
   endgenerate

   // Instantiate the ALUs

   generate
      for (i=0; i < `Nalu; i=i+1) begin : add_array
	 xalu alu (
		   .clk(clk),
		   .rst((init_bit&rw_data_to_wr[`mem_idx-2*`Nmem-i-1])),

		   //controller interface
		   .rw_req(fu_regf_req[`salu0+i-4]),
		   .rw_rnw(rw_rnw),
		   .rw_data_to_wr(rw_data_to_wr),

		   .data_in_bus(data_bus),
		   .alu_result(data_bus[`DATA_BITS - (`salu0+i-1)*`DATA_W-1 -: `DATA_W]),
		   .c_out(carry[i]),
		   
		   .configdata(config_reg_shadow[`ALU0_CONFIG_OFFSET-i*`ALU_CONFIG_BITS-1 -: `ALU_CONFIG_BITS])
		   
		   );
      end
   endgenerate

   // Instantiate the multipliers

   generate
      for (i=0; i < `Nmul; i=i+1) begin : mult_array
	 xmult mult (
		     .clk(clk),
		     .rst((init_bit&rw_data_to_wr[`mem_idx-2*`Nmem-`Nalu-i-1])),

		     //controller interface
		     .rw_req(fu_regf_req[`smul0+i-4]),
		     .rw_rnw(rw_rnw),
		     .rw_data_to_wr(rw_data_to_wr),

		     //data io
		     .data_in_bus(data_bus),
		     .product(data_bus[`DATA_BITS - (`smul0+i-1)*`DATA_W-1 -: `DATA_W]),
		     
		     //configuration data
		     .configdata(config_reg_shadow[`MULT0_CONFIG_OFFSET-i*`MULT_CONFIG_BITS-1 -: `MULT_CONFIG_BITS])

		     );
      end
   endgenerate

   // Instantiate the barrel shifters

   generate
      for (i=0; i < `Nbs; i=i+1) begin : bs_array
	 xbs bs (
		 .clk(clk),
		 .rst((init_bit&rw_data_to_wr[`mem_idx-2*`Nmem-`Nalu-`Nmul-i-1])),

		 //controller interface
		 .rw_req(fu_regf_req[`sbs0+i-4]),
		 .rw_rnw(rw_rnw),
		 .rw_data_to_wr(rw_data_to_wr),

		 .data_in_bus(data_bus),
		 .data_out(data_bus[`DATA_BITS-(`sbs0+i-1)*`DATA_W-1 -: `DATA_W]),
		 
		 .configdata(config_reg_shadow[`BS0_CONFIG_OFFSET-i*`BS_CONFIG_BITS-1 -: `BS_CONFIG_BITS])

		 );
      end

   endgenerate

   always @ (posedge clk) begin
	  if (en)
      state <= state_nxt;
   end 
   
endmodule
