/* ****************************************************************************
 This Source Code Form is subject to the terms of the
 Open Hardware Description License, v. 1.0. If a copy
 of the OHDL was not distributed with this file, You
 can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

 Description: 

 Copyright (C) 2014 Authors

 Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
            Andre Lopes <andre.a.lopes@netcabo.pt>
 ***************************************************************************** */

`timescale 1ns / 1ps
`include "xdefs.v"
`include "xmem_map.v"

module xctrl(
	     input 			   clk,
	     input 			   rst,

	     //prog memory interface 
	     input [`DATA_W-1:0]           instruction,
	     output reg [`ADDR_W-1:0] 	   pc,

	     //read/write interface 
	     output reg 		             rw_req,
	     output reg 		   		     rw_rnw,
	     output reg [`INT_ADDR_W-1:0]    rw_addr,
	     input [`DATA_W-1:0] 	   	     data_to_rd,
	     output reg [`DATA_W-1:0] 	     data_to_wr

	     );

 	//controller states
 	parameter IDLE=2'b00, PROGA=2'b01, EXE=2'b11;
 
 	reg [`DATA_W-1:0] regA;
 	reg [`DATA_W-1:0] regA_nxt;
 	reg [`INT_ADDR_W-1:0] addrint_reg;
 	reg [`ADDR_W-1:0] pc_nxt;
 	reg [1:0]          state,state_nxt;
 	reg	[`DATA_W-1:0] prog_addr;
 	
	//instruction fields
 	wire [`OPCODESZ-1 :0] opcode;
 	wire [`INT_ADDR_W-1:0] addrint;
 	wire [`TRFSZ_W-1:0] size;
 	wire [`DATA_W-1:0] imm;
 	
 	//instruction field assignment
	assign opcode = instruction[`DATA_W-1 -:`OPCODESZ];
 	assign addrint = instruction[`INT_ADDR_W-1:0];
 	assign size = instruction[`INT_ADDR_W+`TRFSZ_W-1:`INT_ADDR_W];
 	assign imm = {4'b0,instruction[`DATA_W-5:0]};

  
   //registers
   always @ (posedge clk, posedge rst)
     if(rst) begin 
		pc = `ADDR_W'h0;
		regA = `DATA_W'h0;
		state = IDLE;
		addrint_reg = `INT_ADDR_W'h0;
		prog_addr=`DATA_W'h0;
     end else begin
		regA = regA_nxt;
		state = state_nxt;
		pc = pc_nxt;
		addrint_reg = addrint;
	 end 	
   
   always @ (*) begin 
	  state_nxt=state;
	  regA_nxt=regA;
	  pc_nxt=pc;
      rw_req = 1'b0;
      rw_rnw = 1'b1;
      rw_addr = addrint;
      data_to_wr = regA;
      
		case (state)
			IDLE: begin 
				rw_addr=`PROG_SIZE_REG;
				rw_req=1'b1;				
				if(data_to_rd != 0) 
					state_nxt=PROGA;
			end	

			PROGA: begin
				rw_addr=`PROG_ADDR_REG;
				rw_req=1'b1;
				state_nxt=EXE;
				if(data_to_rd!=prog_addr) begin
					prog_addr=data_to_rd;
					pc_nxt=11'h0;
				end else 
					pc_nxt=11'h100;
			end

			EXE: begin
				case (opcode)
					`RDW: begin 
						rw_req = 1'b1;
						regA_nxt = data_to_rd;
						pc_nxt=pc+1;					
					end	
					`WRW: begin 
						rw_req = 1'b1;
						rw_rnw = 1'b0;
						pc_nxt=pc+1;											
					end	
					`BEQ: begin
						regA_nxt = regA - `DATA_W'd1;
						if ( regA[31:0] == `DATA_W'd0 )
							pc_nxt = addrint[`ADDR_W-1:0];
						else						
							pc_nxt=pc+1;											
					end		
					`BNEQ: begin
						regA_nxt = regA - `DATA_W'd1;
						if ( regA[31:0] != `DATA_W'd0 )
							pc_nxt = addrint[`ADDR_W-1:0];
						else
						pc_nxt=pc+1;											
					end		
					`LDI: begin
						regA_nxt=imm;
						pc_nxt=pc+1;
					end		
					`ADD: begin 
						regA_nxt = regA + data_to_rd;
						pc_nxt=pc+1;					
					end 
					`SUB: begin 
						regA_nxt = regA - data_to_rd;
						pc_nxt=pc+1;					
					end
					`ADDI: begin
						regA_nxt= regA + imm;
						pc_nxt=pc+1;
					end
					`AND: begin 
						regA_nxt = regA && data_to_rd;
						pc_nxt=pc+1;					
					end 
					`NOP: begin 
						pc_nxt=pc+1;
					end 
					`STOP: begin
						state_nxt = IDLE;
						rw_req = 1'b1;
						rw_rnw = 1'b0;
						rw_addr = `STATUS_REG;
						data_to_wr = `DATA_W'h0;
					end		
					default: 
						pc_nxt = pc;
						
				endcase
			end 
 		endcase

	end 			
			
endmodule
