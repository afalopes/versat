/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: memory map as viewed from the xctrl block

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>

***************************************************************************** */


`include "./xdefs.v"

`define CTRL_REGF_BASE 0
`define STATUS_REG 0 
`define PROG_ADDR_REG 1
`define PROG_SIZE_REG 2
`define ARGC_REG 3
`define ARGV_REG 4

`define DMA_BASE 64
`define DMA_CTRL_REG 64
`define DMA_STATUS_REG 65
`define DMA_EXT_ADDR 66 

`define PROG_MEM_BASE 2048

`define CONF_MEM_BASE 4096

`define CLEAR_CONFIG_ADDR 6143

`define ENG_BASE 6144

`define ENG_CTRL_REG `ENG_BASE

`define ENG_STATUS_REG `ENG_BASE+1

`define ENG_FU_BASE (`ENG_BASE+2**`N_W) //6144+32

`define MEM0A `ENG_FU_BASE
`define MEM0B (`MEM0A+1)
`define MEM1A (`MEM0B+1)
`define MEM1B (`MEM1A+1)
`define MEM2A (`MEM1B+1)
`define MEM2B (`MEM2A+1)
`define MEM3A (`MEM2B+1)
`define MEM3B (`MEM3A+1)
`define ALU0 (`MEM3B+1)
`define ALU1 (`ALU0+1)
`define ALU2 (`ALU1+1)
`define ALU3 (`ALU2+1)
`define MULT0 (`ALU3+1)
`define MULT1 (`MULT0+1)
`define MULT2 (`MULT1+1)
`define MULT3 (`MULT2+1)
`define BS0 (`MULT3+1)
`define BS1 (`BS0+1)





`define ENG_MEM_BASE `ENG_BASE+2048 //8192


//Memory configuration offsets
`define MEM_CONF_ITER_OFFSET 0
`define MEM_CONF_PER_OFFSET 1
`define MEM_CONF_DUTY_OFFSET 2
`define MEM_CONF_SELA_OFFSET 3
`define MEM_CONF_START_OFFSET 4
`define MEM_CONF_INCR_OFFSET 5
`define MEM_CONF_DELAY_OFFSET 6
`define MEM_CONF_RVRS_OFFSET 7
`define MEM_CONF_OFFSET 8

//ALU configuration offsets
`define ALU_CONF_FNS_OFFSET 0
`define ALU_CONF_SELA_OFFSET 1
`define ALU_CONF_SELB_OFFSET 2
`define ALU_CONF_OFFSET 3

//MULT configuration offsets
`define MUL_CONF_SELA_OFFSET 0
`define MUL_CONF_SELB_OFFSET 1
`define MUL_CONF_LONHI_OFFSET 2
`define MUL_CONF_DIV2_OFFSET 3
`define MUL_CONF_OFFSET 4

//BS configuration offsets
`define BS_CONF_SELA_OFFSET 0
`define BS_CONF_SELB_OFFSET 1
`define BS_CONF_LNA_OFFSET 2
`define BS_CONF_LNR_OFFSET 3
`define BS_CONF_OFFSET 4


//configuration addresses 

`define MEM0A_CONFIG_ADDR `CONF_MEM_BASE 
`define MEM0B_CONFIG_ADDR (`MEM0A_CONFIG_ADDR+`MEM_CONF_OFFSET)
`define MEM1A_CONFIG_ADDR (`MEM0B_CONFIG_ADDR+`MEM_CONF_OFFSET) 
`define MEM1B_CONFIG_ADDR (`MEM1A_CONFIG_ADDR+`MEM_CONF_OFFSET ) 
`define MEM2A_CONFIG_ADDR (`MEM1B_CONFIG_ADDR+`MEM_CONF_OFFSET)
`define MEM2B_CONFIG_ADDR (`MEM2A_CONFIG_ADDR+`MEM_CONF_OFFSET)
`define MEM3A_CONFIG_ADDR (`MEM2B_CONFIG_ADDR+`MEM_CONF_OFFSET)
`define MEM3B_CONFIG_ADDR (`MEM3A_CONFIG_ADDR+`MEM_CONF_OFFSET)

`define ALU0_CONFIG_ADDR  (`MEM3B_CONFIG_ADDR+`MEM_CONF_OFFSET)
`define ALU1_CONFIG_ADDR  (`ALU0_CONFIG_ADDR+`ALU_CONF_OFFSET)
`define ALU2_CONFIG_ADDR  (`ALU1_CONFIG_ADDR+`ALU_CONF_OFFSET)
`define ALU3_CONFIG_ADDR  (`ALU2_CONFIG_ADDR+`ALU_CONF_OFFSET)

`define MULT0_CONFIG_ADDR (`ALU3_CONFIG_ADDR+`ALU_CONF_OFFSET)
`define MULT1_CONFIG_ADDR (`MULT0_CONFIG_ADDR+`MUL_CONF_OFFSET)
`define MULT2_CONFIG_ADDR (`MULT1_CONFIG_ADDR+`MUL_CONF_OFFSET)
`define MULT3_CONFIG_ADDR (`MULT2_CONFIG_ADDR+`MUL_CONF_OFFSET)

`define BS0_CONFIG_ADDR (`MULT3_CONFIG_ADDR+`MUL_CONF_OFFSET)
`define BS1_CONFIG_ADDR (`BS0_CONFIG_ADDR+`BS_CONF_OFFSET)



