/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
             Joao Dias Lopes <joao.d.lopes91@gmail.com>

***************************************************************************** */

`timescale 1ns / 1ps
`include "./xdefs.v"
`include "./xmem_map.v"
`include "./xaludefs.v"

module xdata_eng_tb;

	//parameters 
	parameter clk_period = 20; //ns
        
	// Inputs
	reg clk;
	reg en;
	reg [`CONFIG_BITS-1:0] config_bus;
	reg rw_req;
	reg rw_rnw;
	reg [`INT_ADDR_W-1:0] rw_addr;
	reg [`DATA_W-1:0] rw_data_to_wr;
        reg dma_req;
	reg dma_rnw;
	reg [`INT_ADDR_W-2:0] dma_addr;
	reg [`DATA_W-1:0] dma_data_in;

	// Outputs
	wire [`DATA_W-1:0] rw_data_to_rd;
	
	//configs 
	reg [`ALU_FNS_W-1:0] alu_fns;
	reg [`N_W-1:0] sela;
	reg [`N_W-1:0] selb;
	
	reg  [`ADDR_W-1:0] iterations;
	reg [`PERIOD_W-1:0] period;
	reg [`PERIOD_W-1:0] duty;

	reg [`ADDR_W-1:0] start;
	reg [`ADDR_W-1:0] incr;
	reg [`PERIOD_W-1:0] delay;
	reg reverse;
        
        //signals
        real x_r;
        real y_r;
        reg signed [`DATA_W-1:0] x;
        reg signed [`DATA_W-1:0] y;        
	
	// Instantiate the Unit Under Test (UUT)
	xdata_eng uut (
		.clk(clk), 
		.en(en),
		.config_bus(config_bus), 
		.rw_req(rw_req), 
		.rw_rnw(rw_rnw), 
		.rw_addr(rw_addr), 
		.rw_data_to_wr(rw_data_to_wr), 
		.rw_data_to_rd(rw_data_to_rd),
		.dma_req(dma_req),
		.dma_rnw(dma_rnw),
		.dma_addr(dma_addr),
		.dma_data_in(dma_data_in)
	);

	initial begin
		$dumpfile("xdata_eng.vcd");
      		$dumpvars();
		// Initialize Inputs
		clk = 1;
		en = 1;
		config_bus = 0;
		rw_req = 0;
		rw_rnw = 1;
		rw_addr = 0;
		rw_data_to_wr = 0;
	        dma_req = 0;
	        dma_rnw = 1;

		// configure alu3
		#(clk_period + 1)      
		alu_fns = `ALU_ADD;
		sela = `salu0;
		selb = `salu1;
		
		config_bus[`ALU3_CONFIG_OFFSET -: `ALU_CONFIG_BITS] = 
					{sela, selb, alu_fns};

		config_bus[`MULT0_CONFIG_OFFSET-1 -: `MULT_CONFIG_BITS] = 
					{sela, selb, 2'b11};
					

		// write 10 to output of alu0
		rw_addr = `ENG_FU_BASE + `salu0;
		rw_req = 1;
		rw_rnw = 0;
		rw_data_to_wr = 10;
		
		// write 10 to output of alu1
		#clk_period				
		rw_addr = `ENG_FU_BASE + `salu1;
		
		//read alu2 output
		#clk_period		
		rw_rnw = 1;
		rw_addr = `ENG_FU_BASE + `salu2;
		
		//read mult0 output 
		#clk_period		
		rw_addr = `ENG_FU_BASE + `smul0;
		
		#clk_period
		//global configuration reset
		config_bus = 0;
	        
		//setup memory 0 for reading vector a and b
		sela = `sdisabled;
		selb = `sdisabled;
		start = 0;
		incr = 1;
		delay = 0;
		reverse = 0;
	        iterations = 20;
		period = 1;
		duty = 1;
		//setup port A
		config_bus[`MEM0A_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, sela, start, incr, delay, reverse};
	
		start = 20;
		//setup port B
		config_bus[`MEM0B_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, selb, start, incr, delay, reverse};
    
	        //setup memory 1 for storing vector a + b
		sela = `salu3;
		start = 0;
		incr = 1;
		delay = 2;
		reverse = 0;
		//setup port A
		config_bus[`MEM1A_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{sela, start, incr, delay, reverse};

	        //setup port B? disabled?

	        //setup alu3 for adding a and b 
		alu_fns = `ALU_ADD;
		sela = `smem0A;
		selb = `smem0B;
		
		config_bus[`ALU3_CONFIG_OFFSET-1 -: `ALU_CONFIG_BITS] = 
					{sela, selb, alu_fns};

					
/*		//init engine
		rw_addr = `ENG_CTRL_REG;
		rw_rnw = 0;
		rw_data_to_wr = 1; //init
		
		#clk_period 
		rw_data_to_wr = 2; //run


		#clk_period 		
		rw_req = 0;
*/
	        //product
	        #clk_period 
/*		//global configuration
		iterations = 10;
		period = 1;
		duty = 1;
		config_bus[`CONFIG_BITS-1 -: `GLOBAL_CONFIG_BITS] = 
			{iterations, period, duty};

*/
		//setup memory 2 for reading vector a and b
		sela = `sdisabled;
		selb = `sdisabled;
		start = 0;
		incr = 2;
		delay = 0;
		reverse = 0;
		iterations = 20;
		period = 1;
		duty = 1;
		//setup port A
		config_bus[`MEM2A_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, sela, start, incr, delay, reverse};
	
		start = 1;
		//setup port B
		config_bus[`MEM2B_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, selb, start, incr, delay, reverse};
    
	        //setup memory 3 for storing vector a * b
		sela = `smul1;
		start = 0;
		incr = 1;
		delay = 2;
		reverse = 0;
		iterations = 20;
		period = 1;
		duty = 1;
		//setup port A
		config_bus[`MEM3A_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, sela, start, incr, delay, reverse};

	        //setup port B? disabled?

	        //setup mul1 for multiplying a and b 
		sela = `smem2A; //inc of 2 because there are 2 ports per memory
		selb = `smem2B;
		
		config_bus[`MULT1_CONFIG_OFFSET-1 -: `MULT_CONFIG_BITS] = 
					{sela, selb, 2'b11};

					
		//init engine
		rw_addr = `ENG_CTRL_REG;
		rw_rnw = 0;
		rw_data_to_wr = 1; //init
		
		#clk_period 
		rw_data_to_wr = 2; //run


		#clk_period 		
		rw_req = 0;
	        
	        //dot product
	        #(25*clk_period) //wait for eng_done, so there are no operations ongoing
	        //global configuration reset
                config_bus = 0;
		
	        //setup memory 0 for reading vector a and b
		sela = `sdisabled;
		selb = `sdisabled;
		start = 0;
		incr = 2;
		delay = 0;
		reverse = 0;
	        iterations = 20;
		period = 1;
		duty = 1;
		//setup port A
		config_bus[`MEM0A_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, sela, start, incr, delay, reverse};
	
		start = 1;
		//setup port B
		config_bus[`MEM0B_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, selb, start, incr, delay, reverse};
    
	        //setup memory 2 for storing vector a . b
		sela = `salu1;
		start = 0;
		incr = 1;
		delay = 3;
		reverse = 0;
	        iterations = 20;
		period = 1;
		duty = 1;
		//setup port A
		config_bus[`MEM2A_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, sela, start, incr, delay, reverse};

	        //setup port B? disabled?

	        //setup mul0 for multiplying a and b 
		sela = `smem0A;
		selb = `smem0B;
		
		config_bus[`MULT0_CONFIG_OFFSET-1 -: `MULT_CONFIG_BITS] = 
					{sela, selb, 2'b11};
	        
	        //setup alu1 for adding mult0 and reg
	        sela = `smul0;
	        selb = `salu1;
	        alu_fns = `ALU_ADD;
	        
	        config_bus[`ALU1_CONFIG_OFFSET-1 -: `ALU_CONFIG_BITS] =
       				        {sela, selb, alu_fns};
	        
					
		//init engine
		rw_addr = `ENG_CTRL_REG;
		rw_rnw = 0;
		rw_data_to_wr = `DATA_W'h00000FFD; //init - new configuration of reset
	        rw_req = 1;
		
		#(2*clk_period) //because of the memory delay, the init_bit needs at least 2 clock ticks to reset functional units properly
		rw_data_to_wr = `DATA_W'h00000002; //run 		
	        
	        #clk_period
	        rw_req = 0;
	        
	        //filter LPB implementation
	        #(26*clk_period) //wait for eng_done
	        //global configuration reset
	        config_bus = 0;
		
	        //setup alu1 for reg constant
	        sela = `salu1;
	        selb = `salu1;
	        alu_fns = `ALU_LOGIC_AND;
	        
	        config_bus[`ALU1_CONFIG_OFFSET-1 -: `ALU_CONFIG_BITS] =
       				        {sela, selb, alu_fns};
	        
	        //setup alu2 for reg constant
	        sela = `salu2;
	        selb = `salu2;
	        alu_fns = `ALU_LOGIC_AND;
	        
	        config_bus[`ALU2_CONFIG_OFFSET-1 -: `ALU_CONFIG_BITS] =
       				        {sela, selb, alu_fns};
	        
	        // write 0.9 to output of alu1
		rw_addr = `ENG_FU_BASE + `salu1;
		rw_req = 1;
		rw_rnw = 0;
		rw_data_to_wr = `DATA_W'h73333333; //0.9 in q1.31 format	        
	        
	        #clk_period
	        // write 0.1 to output of alu2
		rw_addr = `ENG_FU_BASE + `salu2;
		rw_data_to_wr = `DATA_W'h0CCCCCCD; //0.1 in q1.31 format
	        
	        #clk_period
		rw_req = 0;
	        
	        //setup memory 0 for reading signals x1 and x2
		sela = `sdisabled;
		selb = `sdisabled;
		start = 40;
		incr = 1;
		delay = 0;
		reverse = 0;
	        iterations = 52;
		period = 4;
		duty = 1;
		//setup port A
		config_bus[`MEM0A_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, sela, start, incr, delay, reverse};
	
		//setup port B
		start = 92;
	        config_bus[`MEM0B_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, selb, start, incr, delay, reverse};
	        
	        //setup alu3 for adding x1 and x2 
		alu_fns = `ALU_ADD;
		sela = `smem0A;
		selb = `smem0B;
		
		config_bus[`ALU3_CONFIG_OFFSET-1 -: `ALU_CONFIG_BITS] = 
					{sela, selb, alu_fns};
	       
	        //setup memory 1 for storing result of filter
		sela = `salu0;
	        selb = `sdisabled;
		start = 41;
		incr = 1;
		delay = 4;
		reverse = 0;
	        iterations = 52;
		period = 4;
		duty = 1;
		//setup port A
		config_bus[`MEM1A_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, sela, start, incr, delay, reverse};
	        
	        //setup port B
	        delay = 1;
	        start = 40;
	        config_bus[`MEM1B_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, selb, start, incr, delay, reverse};
	        
	        //setup mul0 for multiplying alu0 and alu1 
		sela = `salu1;
		selb = `smem1B;
		
		config_bus[`MULT0_CONFIG_OFFSET-1 -: `MULT_CONFIG_BITS] = 
					{sela, selb, 2'b00};
                
	        //setup mul1 for multiplying alu2 and alu0 
		sela = `salu2;
		selb = `salu3;
		
		config_bus[`MULT1_CONFIG_OFFSET-1 -: `MULT_CONFIG_BITS] = 
					{sela, selb, 2'b00};
	        
	        //setup alu0 for adding mult0 and mult1
	        sela = `smul0;
	        selb = `smul1;
	        alu_fns = `ALU_ADD;
	        
	        config_bus[`ALU0_CONFIG_OFFSET-1 -: `ALU_CONFIG_BITS] =
       				        {sela, selb, alu_fns};
	        
					
		//init engine
		rw_addr = `ENG_CTRL_REG;
		rw_rnw = 0;
		rw_data_to_wr = `DATA_W'h000FF9FD; //init
	        rw_req = 1;
		
		#(3*clk_period) //because of the memory delay, the init_bit needs at least 2 clock ticks to reset functional units properly
		rw_data_to_wr = `DATA_W'h00000002; //run 		
	        
	        #clk_period
	        rw_req = 0;
	        
	        #(210*clk_period) //wait for results to display results
	        //global configuration reset
                config_bus = 0;
		
	        //setup memory 1 for reading signal y
		sela = `sdisabled;
		selb = `sdisabled;
		start = 41;
		incr = 1;
		delay = 0;
		reverse = 0;
	        iterations = 52;
		period = 1;
		duty = 1;
		//setup port A
		config_bus[`MEM1A_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, sela, start, incr, delay, reverse};
	        
	        //setup port B? disabled?
	        incr = 0;
	        config_bus[`MEM1B_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, selb, start, incr, delay, reverse};
	        
		//setup memory 0 for reading signals x1 and x2
		sela = `sdisabled;
		selb = `sdisabled;
		start = 40;
		incr = 1;
		delay = 0;
		reverse = 0;
	        iterations = 52;
		period = 1;
		duty = 1;
		//setup port A
		config_bus[`MEM0A_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, sela, start, incr, delay, reverse};
	
		start = 92;
		//setup port B
		config_bus[`MEM0B_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, selb, start, incr, delay, reverse};
	        
	        //setup alu3 for adding x1 and x2 
		alu_fns = `ALU_ADD;
		sela = `smem0A;
		selb = `smem0B;
		
		config_bus[`ALU3_CONFIG_OFFSET-1 -: `ALU_CONFIG_BITS] = 
					{sela, selb, alu_fns};
	        
	        //setup alu0 for read signal y 
		alu_fns = `ALU_ADD;
		sela = `smem1A;
		selb = `salu2;
		
		config_bus[`ALU0_CONFIG_OFFSET-1 -: `ALU_CONFIG_BITS] = 
					{sela, selb, alu_fns};

	        //setup alu2 for reg constant 0
	        sela = `salu2;
	        selb = `salu2;
	        alu_fns = `ALU_LOGIC_AND;
	        
	        config_bus[`ALU2_CONFIG_OFFSET-1 -: `ALU_CONFIG_BITS] =
       				        {sela, selb, alu_fns};
	        
	        //init engine
		rw_addr = `ENG_CTRL_REG;
		rw_rnw = 0;
		rw_data_to_wr = `DATA_W'h000FFFFD; //init
	        rw_req = 1;
		
		#(3*clk_period) //because of the memory delay, the init_bit needs at least 2 clock ticks to reset functional units properly
		rw_data_to_wr = `DATA_W'h00000002; //run 		
	        
	        #clk_period
	        rw_req = 0;
	        
	        //simulation time 7000 ns
	        #(7000-280*clk_period-1) $finish;
	        
	end
      
	always 
		#(clk_period/2) clk = ~clk;
        
        always @ (*) begin
	   x = uut.data_bus[`DATA_BITS - `salu3*`DATA_W-1 -: `DATA_W];
	   y = uut.data_bus[`DATA_BITS - `salu0*`DATA_W-1 -: `DATA_W];
	   x_r = x;
	   y_r = y;

	end
		
endmodule

