/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
             Andre Lopes <andre.a.lopes@netcabo.pt>
***************************************************************************** */

`timescale 1ns / 1ps
`include "xdefs.v"
`include "xmem_map.v"

module xtop_tb;

	//parameters 
	parameter clk_period = 20;

    reg clk;
    reg rst;
	reg en;
	 
	//control slave interface to master CPU 
	 reg                          creq;
	 wire                         cack;
	 reg                          crnw;	 
	 reg  [`CTRL_REGF_ADDR_W-1:0] caddr;
	 reg  [`DATA_W-1:0]           cdata_in;
	 wire [`DATA_W-1:0]           cdata_out;

	//master interface to external memory
	 wire                 mreq;
	 wire                  ack;
	 wire                 mrnw;	
	 wire [`DATA_W-1:0]   maddr;
	 wire  [`DATA_W-1:0]   mdata_in;
	 wire [`DATA_W-1:0]   mdata_out;
	 wire [3:0]  		    msel;
	 wire [1:0]  		    mbte;
	 wire [2:0]  		    mcti;
	 wire        		    mcyc;
 
	//data direct IO
/*	 input                data_in_req,
	 output               data_in_rdy,
	 input [`DATA_W-1:0]  data_in,
	 output               data_out_req,
	 input                data_out_rdy,	 
	 output [`DATA_W-1:0] data_out*/

	// Instantiate the Unit Under Test (UUT)
	xtop uut (
		.clk(clk), 
		.rst(rst),
		.en(en),
		// control slave interface
		.creq(creq),
		.cack(cack),
		.crnw(crnw),	 
		.caddr(caddr),
		.cdata_in(cdata_in),
		.cdata_out(cdata_out),
		//external memory interface
		.mreq(mreq),
		.mack(ack),
		.mrnw(mrnw),	
		.maddr(maddr),
		.mdata_in(mdata_in),
	 	.mdata_out(mdata_out),
		.msel(msel),
		.mbte(mbte),
		.mcti(mcti),
		.mcyc(mcyc)
		//data direct IO
/*		.data_in_req(data_in_req),
		.data_in_rdy(data_in_rdy),
		.data_i(data_i),
		.data_out_req(data_out_req),
		.data_out_rdy(data_out_rdy),	 
		.data_out(data_out)*/
		);

	ram_wb_b3 mem_ext (
		.wb_clk_i(clk),
		.wb_rst_i(rst),
		.wb_adr_i(maddr),
		.wb_dat_i(mdata_out),
		.wb_sel_i(msel),
		.wb_we_i(~mrnw),
		.wb_bte_i(mbte),
		.wb_cti_i(mcti),
		.wb_cyc_i(mcyc),
		.wb_stb_i(mreq),
		.wb_ack_o(ack),
		.wb_dat_o(mdata_in)
	);
	initial begin
		$dumpfile("xtop.vcd");
      		$dumpvars();
		// Initialize Inputs
		clk=1; 
		rst=0;
		en=1;
		#(clk_period)
		rst=1;

		#(clk_period)
		rst=0;

		#(clk_period)
		creq=1'b1;
		crnw=1'b0;
		caddr=`PROG_SIZE_REG;
		cdata_in=2000;
		#(clk_period)
		#(clk_period)
		creq=1'b1;
		crnw=1'b0;
		caddr=`PROG_ADDR_REG;
		cdata_in=3000;
		#(clk_period)
		#(clk_period)
		creq=1'b0;
		crnw=1'b1;
		// Wait 100 ns for global reset to finish
		#100;
        	
		// Add stimulus here
		#(2000) $finish;

	end
      	always 
		#(clk_period/2) clk = ~clk;
	
endmodule

