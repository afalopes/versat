/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
             Joao Dias lopes <joao.d.lopes91@gmail.com>

***************************************************************************** */

`timescale 1ns / 1ps
`include "xdefs.v"

module xconf_mem_slice(
    input 		     clk,
    input 		     en,
    input 		     dma_req,
    input 		     dma_rw_rnw,
    input 		     conf_req,
    input 		     conf_rw_rnw,
    input [`CONF_ADDR_W-1:0] addr,
    input [`DATA_W-1:0]      dma_data_in,
    output reg [`DATA_W-1:0] dma_data_out,
    input [`DATA_W-1:0]      conf_data_in,
    output reg [`DATA_W-1:0] conf_data_out
);

   reg [`DATA_W-1:0] conf_mem_slice [2**`CONF_ADDR_W-1:0];
   
   wire 	     en_dma;
   wire 	     en_conf;
   
   assign en_dma = en & dma_req;
   assign en_conf = en & conf_req;
   
   always @(posedge clk) begin
      if (en_dma) begin
	 if(dma_rw_rnw)
	   dma_data_out <= conf_mem_slice[addr];
	 else
	   conf_mem_slice[addr] <= dma_data_in;
      end
   end

   always @(posedge clk) begin
      if (en_conf) begin
	 if(conf_rw_rnw)
	   conf_data_out <= conf_mem_slice[addr];
	 else
	   conf_mem_slice[addr] <= conf_data_in;
      end
   end

endmodule
