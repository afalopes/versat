/* ****************************************************************************
 This Source Code Form is subject to the terms of the
 Open Hardware Description License, v. 1.0. If a copy
 of the OHDL was not distributed with this file, You
 can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

 Description: architecture parameters

 Copyright (C) 2014 Authors

 Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
            Joao Dias lopes <joao.d.lopes91@gmail.com>
            Andre Lopes <andre.a.lopes@netcabo.pt>

***************************************************************************** */

//data width
`define DATA_W 32 //bits

//boot ROM data memory address width
`define ROM_ADDR_W 8 //2**8 = 256 boot instructions

//2**14 = 16k internal addresses; see memory map
`define INT_ADDR_W 14 	

//data memory address width
`define ADDR_W 11 //means 2**11 * 4 = 8kB each mem, 32kB for 4 data mems							 

//program starts at 100h
`define PROG_START `ADDR_W'h100

//control reg file
`define CTRL_REGF_ADDR_W 4

//number of units
`define Nmem 4
`define Nalu 4
`define Nmul 4
`define Nbs 2
`define N_W 5 //2**5 = 32: max number of inmux inputs
`define N  (2*`Nmem+`Nalu+`Nmul+`Nbs+3) //actual number of inmux inputs; 
//total = 21;note the 3 special inputs: 0, 1, data_in

`define DATA_BITS `N *`DATA_W //internal data bus

//data bus selection codes
`define sdisabled       0 //0
`define s0 	       	(`sdisabled + 1) //1
`define s1     		(`s0 +1) //2
`define sdata_in	(`s1 +1) //3
`define smem0A 		(`sdata_in + 1) //4
`define smem0B		(`smem0A + 1) //5
`define smem1A 		(`smem0B + 1) //6
`define smem1B		(`smem1A + 1) //7
`define smem2A 		(`smem1B + 1) //8
`define smem2B		(`smem2A + 1) //9
`define smem3A 		(`smem2B + 1) //10
`define smem3B		(`smem3A + 1) //11
`define salu0 		(`smem3B + 1) //12
`define salu1 		(`salu0 + 1) //13
`define salu2 		(`salu1 + 1) //14
`define salu3 		(`salu2 + 1) //15 
`define smul0 		(`salu3 + 1) //16
`define smul1 		(`smul0 + 1) //17
`define smul2 		(`smul1 + 1) //18
`define smul3 		(`smul2 + 1) //19
`define sbs0		(`smul3 + 1) //20
`define sbs1		(`sbs0 + 1) //21

//index of init_bit and run_bit mask
`define mem_idx (`salu0+2*`Nmem) //20

//configuration memory 
`define CONF_ADDR_W 6 //means 2**6 configurations
`define N_CONF_SLICE_W 5 //means 2**5 = 32 conf mem slices, just 18 needed
`define N_CONF_SLICES (2**`N_CONF_SLICE_W-14) //18 slices

`define PERIOD_W 5  //LOG2 of max period and duty cicle

//MEM port config bits: input selection bits + START+INCR+DELAY+RNW+REVERSE
//iterations = `ADDR_W
//period = PERIOD_W 5
//duty = PERIOD_W 5
//input selection  bits = N_W
//start width = ADDR_W 
//incr width = ADDR_W 
//delay width = PERIOD_W 
//reverse = 1 
`define MEMP_CONFIG_BITS (`N_W+3*`ADDR_W+3*`PERIOD_W+1) //54

//ALU config bits:
//input selection  bits = 2 * N_W
`define ALU_FNS_W	5
`define ALU_CONFIG_BITS (2*`N_W+`ALU_FNS_W) //15

//MULT config bits:
//input selection bits = 2 * N_W
//lonhi = 1 bit 
//div2 = 1 bit 
`define MULT_CONFIG_BITS (2*`N_W+2) //12

//BS config bits:
//input selection bits = 2 * N_W
// lognarith = 1 bit 
// leftnright = 1 bit
`define BS_CONFIG_BITS (2*`N_W+2) //12

//total config bits 
`define CONFIG_BITS (2*`Nmem*`MEMP_CONFIG_BITS+ `Nalu*`ALU_CONFIG_BITS+`Nmul*`MULT_CONFIG_BITS+ `Nbs*`BS_CONFIG_BITS)

`define MEM0A_CONFIG_OFFSET `CONFIG_BITS
`define MEM0B_CONFIG_OFFSET (`MEM0A_CONFIG_OFFSET-`MEMP_CONFIG_BITS)
`define MEM1A_CONFIG_OFFSET (`MEM0B_CONFIG_OFFSET-`MEMP_CONFIG_BITS) 
`define MEM1B_CONFIG_OFFSET (`MEM1A_CONFIG_OFFSET-`MEMP_CONFIG_BITS) 
`define MEM2A_CONFIG_OFFSET (`MEM1B_CONFIG_OFFSET-`MEMP_CONFIG_BITS)
`define MEM2B_CONFIG_OFFSET (`MEM2A_CONFIG_OFFSET-`MEMP_CONFIG_BITS)
`define MEM3A_CONFIG_OFFSET (`MEM2B_CONFIG_OFFSET-`MEMP_CONFIG_BITS)
`define MEM3B_CONFIG_OFFSET (`MEM3A_CONFIG_OFFSET-`MEMP_CONFIG_BITS)
`define ALU0_CONFIG_OFFSET  (`MEM3B_CONFIG_OFFSET-`MEMP_CONFIG_BITS)
`define ALU1_CONFIG_OFFSET  (`ALU0_CONFIG_OFFSET-`ALU_CONFIG_BITS)
`define ALU2_CONFIG_OFFSET  (`ALU1_CONFIG_OFFSET-`ALU_CONFIG_BITS)
`define ALU3_CONFIG_OFFSET  (`ALU2_CONFIG_OFFSET-`ALU_CONFIG_BITS)
`define MULT0_CONFIG_OFFSET (`ALU3_CONFIG_OFFSET-`ALU_CONFIG_BITS)
`define MULT1_CONFIG_OFFSET (`MULT0_CONFIG_OFFSET - `MULT_CONFIG_BITS)
`define MULT2_CONFIG_OFFSET (`MULT1_CONFIG_OFFSET - `MULT_CONFIG_BITS)
`define MULT3_CONFIG_OFFSET (`MULT2_CONFIG_OFFSET - `MULT_CONFIG_BITS)
`define BS0_CONFIG_OFFSET (`MULT3_CONFIG_OFFSET - `MULT_CONFIG_BITS)
`define BS1_CONFIG_OFFSET (`BS0_CONFIG_OFFSET - `BS_CONFIG_BITS)



  
/*******************************************************************************

 Instructions
 
  opocode: 31:28

 */ 
  
//instruction fields
`define reserved 6
`define OPCODESZ 4
`define TRFSZ_OFFSET (`DATA_W-`reserved-`OPCODESZ) //22
`define TRFSZ_W 8 //bits - means 16 words max depth
`define INT_ADDR_OFFSET (`TRFSZ_OFFSET-`TRFSZ_W) //14

//instructions
`define NOP	4'h0 // No operation
`define RDW	4'h1 // A := (IA) ; PC=PC+1
`define WRW	4'h2 // (IA) := A   PC=PC+1
`define BEQ	4'h3 // (A == 0)? PC := (IA) : PC=PC+1; A = A - 1;
`define BNEQ	4'h4 // (A != 0)? PC := (IA) : PC=PC+1; A = A - 1;
`define LDI     4'h5 // A = A + IMM ; PC=PC+1
`define ADD	4'h6 // A = A + (IA) ; PC=PC+1
`define ADDI	4'h7 // A = A + (IA) ; PC=PC+1
`define SUB	4'h8 // A = A - (IA) ; PC=PC+1
`define AND	4'h9 // A = A & (IA) ; PC=PC+1
`define STOP	4'hA // state := IDLE
