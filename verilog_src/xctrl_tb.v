/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
             Joao Dias Lopes <joao.d.lopes91@gmail.com>
             Andre Lopes <andre.a.lopes@netcabo.pt>

***************************************************************************** */

`timescale 1ns / 1ps
`include "./xdefs.v"

module xctrl_tb;

    //parameters 
	parameter clk_period = 20;

	// Inputs
	reg clk;
	reg rst;
	reg [`DATA_W-1:0] instruction;
    reg [`DATA_W-1:0] data_to_rd;
 
    // Outputs
	wire [`ADDR_W-1:0] pc;
	wire eng_start;
	wire rw_req;
    wire rw_rnw;
    wire [`INT_ADDR_W-1:0] rw_addr;
    wire [`DATA_W-1:0] data_to_wr;


	// Instantiate the Unit Under Test (UUT)
	xctrl uut (
		.clk(clk),
        .rst(rst),
        .instruction(instruction),
        .data_to_wr(data_to_wr),
        .pc(pc),
        .rw_req(rw_req),
        .rw_rnw(rw_rnw),
        .rw_addr(rw_addr),
        .data_to_rd(data_to_rd)
	);

	initial begin
		$dumpfile("xctrl.vcd");
  		$dumpvars();
		// Initialize Inputs
		clk = 0;
		rst = 0;
		instruction = 0;
		data_to_rd=1;
        //data_to wr = `DATA_W'bx;
    	    
		// Wait 20 ns for global reset to finish
        #(clk_period/2) rst = 1;
        #clk_period rst = 0;
            
		// Add stimulus here
        // Test WRW and LDI functions
        #(clk_period)
        instruction[`DATA_W-1-:`OPCODESZ] = `LDI;
		instruction[`INT_ADDR_W-1:0] = 14'd3000;
		instruction[`TRFSZ_OFFSET-1 -:`TRFSZ_W] = 8'd20;        

        #(clk_period)
		instruction[`DATA_W-1 -:`OPCODESZ] = `WRW;
		instruction[`INT_ADDR_W:0]=14'd64;

        // Test load and store functions
        #clk_period
		instruction[`DATA_W-1-:`OPCODESZ] = `LDI;
        instruction[`DATA_W-5:0] = 28'd2000;
        
        #(clk_period)
		instruction[`DATA_W-1 -:`OPCODESZ] = `WRW;
		instruction[`INT_ADDR_W:0]=14'd65;
        
        #clk_period
        instruction[`DATA_W-1-:`OPCODESZ] = `LDI;
        instruction[`DATA_W-`TRFSZ_OFFSET-1-:`TRFSZ_W] = `TRFSZ_W'h40;
        instruction[`DATA_W-`INT_ADDR_OFFSET-1-:`INT_ADDR_W] = `INT_ADDR_W'h1800;
        
        // Simulation time 1000 ns
        #(200) $finish;
        
	end
      	always 
		#(clk_period/2) clk = ~clk;      
endmodule

