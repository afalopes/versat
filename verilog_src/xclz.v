/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>

***************************************************************************** */

`timescale 1ns / 1ps

module xclz(
    input [31:0] data_in,
    output reg [5:0] data_out
    );

	integer signed i;
	reg mark;
	
	always @ (data_in) begin 
		mark = 0;
		data_out = 32;
		for ( i = 31; i >= 0; i = i - 1)  
			if ( data_in[i] == 1'b1 && mark == 0) begin 
				mark = 1;
				data_out = 31-i;
			end
	end 
	
endmodule
