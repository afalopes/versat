/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
             Joao Dias Lopes <joao.d.lopes91@gmail.com>

***************************************************************************** */

`timescale 1ns / 1ps
`include "./xdefs.v"

module xconf_mem_tb;
   
   parameter clk_period = 20;
   
   // Inputs
   reg clk;
   reg en;

   //dma
   reg dma_req;
   reg dma_rw_rnw;
   reg [`DATA_W-1:0] dma_data_in;
   reg [`CONF_ADDR_W+`N_CONF_SLICE_W-1:0] dma_addr;

   //config_reg
   reg conf_req;
   reg conf_rw_rnw;
   reg [`CONFIG_BITS-1:0] conf_data_in;
   reg [`CONF_ADDR_W - 1:0] conf_sel;
   
   // Outputs 
   wire [`DATA_W-1:0] 	    dma_data_out;
   wire [`CONFIG_BITS-1:0]  conf_data_out;
   wire 		    conf_ld;
   
   reg [`CONF_ADDR_W-1:0]    sel;
   
   // Config Bank
   reg [`CONFIG_BITS-1:0] mem [(2**`CONF_ADDR_W)-1:0];
   
   integer 		  i;
   
   // Instantiate the Unit Under Test (UUT)
   xconf_mem uut (
		  .clk(clk),
		  .en(en),
		  .dma_req(dma_req),
		  .dma_rw_rnw(dma_rw_rnw),
		  .dma_addr(dma_addr),
		  .dma_data_in(dma_data_in),
		  .dma_data_out(dma_data_out),
		  .conf_req(conf_req),
		  .conf_rw_rnw(conf_rw_rnw),
		  .conf_sel(conf_sel),
		  .conf_data_in(conf_data_in),
		  .conf_data_out(conf_data_out),
		  .conf_ld(conf_ld)
		  );

   initial begin
      $dumpfile("xconf_mem.vcd");
      $dumpvars();
      $readmemh("xconfigdata.hex", mem, 0, 63);
      
      // Initialize Inputs
      clk = 0;
      en = 1;
      dma_req = 0;
      conf_req = 0;
      dma_rw_rnw = 1;
      conf_rw_rnw = 1;
      
      // test xconfig_mem read from an empty config
      #clk_period
      conf_sel = `CONF_ADDR_W'b0;
      conf_rw_rnw = 1'b1;
      
      #clk_period
      conf_req = 1;

      #clk_period
      conf_req = 0;
      
      // test xconfig_mem write from conf_reg
      #(2*clk_period+1)
      conf_sel = `CONF_ADDR_W'b0;
      conf_rw_rnw = 1'b0;
      conf_data_in = mem[conf_sel];

      #clk_period
      conf_req = 1;

      #clk_period
      conf_req = 0;
      
      // test xconfig_mem read from conf_reg previous write
      #clk_period
      conf_sel = `CONF_ADDR_W'b0;
      conf_rw_rnw = 1'b1;
      
      #clk_period
      conf_req = 1;

      #clk_period
      conf_req = 0;
      
      // test xconf_mem write from dma
      #clk_period
      dma_addr = {`CONF_ADDR_W'b1,`N_CONF_SLICE_W'b0};
      dma_rw_rnw = 0;

      #clk_period
      dma_req = 1;
      
      for (i=0; i < `N_CONF_SLICES; i=i+1) begin
	 sel = dma_addr[`CONF_ADDR_W+`N_CONF_SLICE_W-1 -: `CONF_ADDR_W]; 
	 
	 if (i < (`N_CONF_SLICES - 1))
	   dma_data_in = mem[`CONF_ADDR_W'b1][`CONFIG_BITS-i*`DATA_W-1 -: `DATA_W];
	 else
	   dma_data_in = {mem[`CONF_ADDR_W'b1][`CONFIG_BITS-i*`DATA_W-1 -: (`DATA_W-12)],12'bx};
	 
	 #clk_period
	 
	 dma_addr = dma_addr + 1;
      end
      
      dma_req = 0;
      
      // test xconf_mem read from dma previous write
      #clk_period
      dma_addr = {`CONF_ADDR_W'b1,`N_CONF_SLICE_W'b0};
      dma_rw_rnw = 1;

      #clk_period
      dma_req = 1;
      
      for (i=0; i < `N_CONF_SLICES; i=i+1) begin
	 #clk_period
	 dma_addr = dma_addr + 1;
      end
      
      dma_req = 0;
      
      // Simulation time 1500 ns
      #(1500-50*clk_period-1) $finish;
   end
	
   always 
     #(clk_period/2) clk = ~clk;
      	 
endmodule

