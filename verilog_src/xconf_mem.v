/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
             Joao Dias lopes <joao.d.lopes91@gmail.com>

***************************************************************************** */

`timescale 1ns / 1ps
`include "xdefs.v"

module xconf_mem(
    input 				     clk,
    input 				     en,
    	 
	 //dma port
    input 				     dma_req,
    input 				     dma_rw_rnw,
    input [`CONF_ADDR_W+`N_CONF_SLICE_W-1:0] dma_addr,
    input [`DATA_W-1:0] 		     dma_data_in,
    output reg [`DATA_W-1:0] 		     dma_data_out,
	 
	 //configuration port
    input 				     conf_req,
    input 				     conf_rw_rnw,
    input [`CONF_ADDR_W-1:0] 		     conf_sel,
    input [`CONFIG_BITS-1:0] 		     conf_data_in,
    output [`CONFIG_BITS-1:0] 		     conf_data_out,
    output 				     conf_ld
);

  //slice enable array
  reg [`N_CONF_SLICES-1:0] en_int;
  
  //configuration addr
  reg [`CONF_ADDR_W-1:0] addr;
  
  //data io
  wire [`N_CONF_SLICES*`DATA_W-1:0] data_in;
  wire [`N_CONF_SLICES*`DATA_W-1:0] data_out;
  wire [`N_CONF_SLICES*`DATA_W-1:0] dma_data_out_int;
   
  //load configuration to xconf_reg
  assign conf_ld = conf_req & conf_rw_rnw;
  
  //read/write data
  assign data_in = {conf_data_in,12'bx};
  assign conf_data_out = data_out [`N_CONF_SLICES*`DATA_W-1 -: `CONFIG_BITS];
  
  //slice decoder
  integer i;
  always @ (*) begin
     for (i=0; i < `N_CONF_SLICES; i=i+1)
       if (en_int[i])
	 dma_data_out = dma_data_out_int[`N_CONF_SLICES*`DATA_W-i*`DATA_W-1 -:`DATA_W];
     
     if (dma_req) begin
       for (i=0; i<`N_CONF_SLICES; i=i+1)
	 en_int[i] = ((i == dma_addr[`N_CONF_SLICE_W-1 -:`N_CONF_SLICE_W])? 1'b1: 1'b0) & en;
     end else if (conf_req) begin
       for (i=0; i<`N_CONF_SLICES; i=i+1)
	 en_int[i] = 1'b1;
     end else begin
       for (i=0; i<`N_CONF_SLICES; i=i+1)
	 en_int[i] = 1'b0;
     end
  end
  
  //select configuration
  always @ (*) begin
     if (dma_req)
       addr = dma_addr[`CONF_ADDR_W+`N_CONF_SLICE_W-1 -:`CONF_ADDR_W];

     if (conf_req)
       addr = conf_sel;
  end
  
  //slice array 
  genvar j;
   generate
      for (j=0; j < `N_CONF_SLICES; j=j+1) begin : conf_slice_array
	 
	 xconf_mem_slice xconf_slice (
				      .clk(clk),
				      .en(en_int[j]),
				      .dma_req(dma_req),
				      .dma_rw_rnw(dma_rw_rnw),
				      .conf_req(conf_req),
				      .conf_rw_rnw(conf_rw_rnw),
				      .addr(addr),
				      .dma_data_in(dma_data_in),
				      .dma_data_out(dma_data_out_int[`N_CONF_SLICES*`DATA_W-j*`DATA_W-1 -:`DATA_W]),
				      .conf_data_in(data_in[`N_CONF_SLICES*`DATA_W-j*`DATA_W-1 -:`DATA_W]),
				      .conf_data_out(data_out[`N_CONF_SLICES*`DATA_W-j*`DATA_W-1 -:`DATA_W])
				      );
	 
      end
   endgenerate
endmodule
