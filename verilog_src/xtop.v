/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
             Andre Lopes <andre.a.lopes@netcabo.pt>
***************************************************************************** */

`timescale 1ns / 1ps
`include "xdefs.v"

module xtop(

    input clk,
    input rst,
	input en,
	 
	//control slave interface to master CPU 
	 input                          creq,
	 output                         cack,
	 input                          crnw,	 
	 input  [`CTRL_REGF_ADDR_W-1:0] caddr,
	 input  [`DATA_W-1:0]           cdata_in,
	 output [`DATA_W-1:0]           cdata_out,

	//master interface to external memory
	 output                 mreq,
	 input                  mack,
	 output                 mrnw,	
	 output [`DATA_W-1:0]   maddr,
	 input  [`DATA_W-1:0]   mdata_in,
	 output [`DATA_W-1:0]   mdata_out,
	 output [3:0]  		    msel,
	 output [1:0]  		    mbte,
	 output [2:0]  		    mcti,
	 output        		    mcyc
 
	//data direct IO
/*	 input                data_in_req,
	 output               data_in_rdy,
	 input [`DATA_W-1:0]  data_in,
	 output               data_out_req,
	 input                data_out_rdy,	 
	 output [`DATA_W-1:0] data_out*/
	 
);

// engine configuration bus
wire [`CONFIG_BITS-1:0] config_bus;

//program memory - xctrl interface
wire  [`DATA_W-1:0] instruction;
wire  [`ADDR_W-1:0] pc;

// Internal rw bus (controller managed)
wire                   rw_req; 
wire                   rw_rnw;
wire [`INT_ADDR_W-1:0] rw_addr;
wire [`DATA_W-1:0]     rw_data_to_rd; 
wire [`DATA_W-1:0]     rw_data_to_wr;
 
// Address decoder enable signals
wire               ctrl_regf_req;
wire               eng_req;
wire [`DATA_W-1:0] dma_data_to_rd;
wire [`DATA_W-1:0] ctrl_data_to_rd;
wire [`DATA_W-1:0] eng_data_to_rd;



//instruction memory - dma interface
wire	     			inst_mem_we;

//config memory - dma interface
wire                    dconf_mem_req;
wire                    dconf_mem_rnw;
wire [`ADDR_W-1: 0]     mem_addr;

//config memory - 
wire [`CONFIG_BITS-1:0] conf_out;
wire                    conf_mem_req;

//data engine memory - dma interface
wire 					memrdy;
wire                    dma_req;
wire                    dma_rnw;
wire [`INT_ADDR_W-2:0]  dma_addr;
wire [`DATA_W-1:0]      dma_data_in ;
wire [`DATA_W-1:0]      dma_data_out;



/**************************************************************
*
* code section
**************************************************************/		

	// Instantiate the control reg_file
	xctr_regf ctrl_regf (
		.clk(clk),
		
		.ext_req(creq), 
		.ext_rnw(crnw), 
		.ext_addr(caddr), 
		.ext_data_in(cdata_in), 
		.ext_data_out(cdata_out), 

		.int_req(ctrl_regf_req), 
		.int_rnw(rw_rnw), 
		.int_addr(rw_addr[`CTRL_REGF_ADDR_W-1:0] ), 
		.int_data_in(rw_data_to_wr), 
		.int_data_out(ctrl_data_to_rd)
	);


	// Instantiate the controller
	xctrl controller (
		.clk(clk), 
		.rst(rst),
		
		//prog mem interface
		.instruction(instruction), 
		.pc(pc), 

		// internal rw bus
		.rw_req(rw_req), 
		.rw_rnw(rw_rnw), 
		.rw_addr(rw_addr), 
		.data_to_rd(rw_data_to_rd), 
		.data_to_wr(rw_data_to_wr)
	);

	// Instantiate internal address decoder
	xaddr_decoder addr_decoder (
		.rw_addr(rw_addr), 
       		.rw_req(rw_req),
		.dma_data_to_rd(dma_data_to_rd),
		.ctrl_data_to_rd(ctrl_data_to_rd),
		.eng_data_to_rd(eng_data_to_rd),

		.ctrl_regf_req(ctrl_regf_req),  
		.conf_mem_req(conf_mem_req), 
		.eng_req(eng_req),
		.dma_req(dma_req),
		.data_to_rd(rw_data_to_rd)
	);

	// Instantiate the DMA
	xdma dma (
		.clk(clk), 
		.rst(rst), 
		
		//control interface
		.rw_req(dma_req), 
		.rw_addr(rw_addr), 
		.data_to_rd(dma_data_to_rd),
		.data_to_wr(rw_data_to_wr),

		//external interface 
		.ext_req(mreq), 
        	.ext_cyc(mcyc),
		.ext_ack(mack), 
		.ext_rnw(mrnw), 
		.ext_addr(maddr), 
		.ext_data_in(mdata_in), 
		.ext_data_out(mdata_out),
		.ext_sel(msel),
		.ext_bte(mbte),
		.ext_cti(mcti),
		
		//internal interface
        	.mem_rdy(memrdy),
		.int_en(dma_req), 
		.int_rnw(dma_rnw), 
		.int_addr(dma_addr), 
		.int_data_in(dma_data_in), 
		.int_data_out(dma_data_out),
		.mem_addr(mem_addr),
		.conf_req(dconf_mem_req),
		.conf_rnw(dconf_mem_rnw),
        	.inst_mem_we(inst_mem_we)
	);

		// Instantiate the instruction memory
	xprog_mem instr_mem (
		.clk(clk),
		.en(en),
		.pc(pc),
       		.instruction(instruction),
 	 	.dma_wnr(inst_mem_we),
     		.dma_addr(mem_addr),
     		.dma_data(dma_data_out)

		);

		// Instantiate the configuration memory
	xconf conf_mem (
		.clk(clk),
		.en(en),
		.rst(rst),
  		//control port (accessed by dma and controller)
		.dma_req(dconf_mem_req),
		.dma_rw_rnw(dconf_mem_rnw),
		.dma_addr(mem_addr), 
		.dma_data_in(dma_data_out), 
		.dma_data_out(dma_data_in), 
		
		//configuration port (managed by controller, read by engine)
		.conf_req(conf_mem_req),
		.conf_rw_rnw(rw_rnw),
		.conf_addr(rw_addr),
    		.conf_out(conf_out),
		.conf_word(rw_data_to_wr)
		
	);

	// Instantiate the data plane engine
	xdata_eng data_eng (
		.clk(clk), 
		.en(en),		
		.rw_req(rw_req), 
		.rw_rnw(rw_rnw), 
		.rw_addr(rw_addr),
		.rw_data_to_wr(rw_data_to_wr),
		.rw_data_to_rd(eng_data_to_rd),
		.config_bus(conf_out),
 		.dma_req(dma_req),
 		.dma_rnw(dma_rnw),
 		.dma_addr(dma_addr),
		.dma_data_in(dma_data_out),
	 	.dma_data_out(dma_data_in),
		.mem_rdy(memrdy)
	);


endmodule
