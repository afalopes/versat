/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Joao Dias lopes <joao.d.lopes91@gmail.com>

***************************************************************************** */

`timescale 1ns / 1ps
`include "xdefs.v"

module xconf(
	     //xconf_mem ports
	     input 				      clk,
	     input 				      en,
    	     input 				      dma_req,
	     input 				      dma_rw_rnw,
	     input [`CONF_ADDR_W+`N_CONF_SLICE_W-1:0] dma_addr,
	     input [`DATA_W-1:0] 		      dma_data_in,
	     output [`DATA_W-1:0] 		      dma_data_out,
	     input 				      conf_req	,
	     input 				      conf_rw_rnw,
	     
	     //xconf_reg ports
	     input 				      rst,
	     input [`INT_ADDR_W-1:0] 		      conf_addr,
	     input [`DATA_W-1:0] 		      conf_word,
	     output [`CONFIG_BITS-1:0] 		      conf_out
	     );
   
   wire 					      conf_ld;
   wire [`CONFIG_BITS-1:0] 			      conf_in;
   
   xconf_mem conf_mem(
		      .clk(clk),
		      .en(en),
		      .dma_req(dma_req),
		      .dma_rw_rnw(dma_rw_rnw),
		      .dma_addr(dma_addr),
		      .dma_data_in(dma_data_in),
		      .dma_data_out(dma_data_out),
		      .conf_req(conf_req),
		      .conf_rw_rnw(conf_rw_rnw),
		      .conf_sel(conf_addr[`CONF_ADDR_W-1:0]),
		      .conf_data_in(conf_out),
		      .conf_data_out(conf_in),
		      .conf_ld(conf_ld)
		      );
   
   xconf_reg xconf_reg(
		      .clk(clk),
		      .rst(rst),
		      .conf_req(conf_req),
		      .conf_rw_rnw(conf_rw_rnw),
		      .conf_addr(conf_addr),
		      .conf_word(conf_word),
		      .conf_in(conf_in),
		      .conf_ld(conf_ld),
		      .conf_out(conf_out)
		      );
   
endmodule
