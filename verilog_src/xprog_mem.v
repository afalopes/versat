/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Joao Dias lopes <joao.d.lopes91@gmail.com>

***************************************************************************** */

`timescale 1ns / 1ps
`include "xdefs.v"

module xprog_mem(
		 //xboot_rom ports
		 input 		      en,
		 
		 //xinstr_mem ports
		 input 		      dma_wnr,
		 input [`ADDR_W-1:0]  dma_addr,
		 input [`DATA_W-1:0]  dma_data,
		 
		 //both
		 input 		      clk,
		 input [`ADDR_W-1:0]  pc,
		 output [`DATA_W-1:0] instruction
		 );
   
   wire [`DATA_W-1:0] 		      data_rom;
   wire [`DATA_W-1:0] 		      data_mem;
   wire 			      req;

   assign instruction = (pc < `PROG_START)? data_rom : data_mem;
   assign req=1'b1;

   xboot_rom boot_rom(
		      .clk(clk),
		      .en(en),
		      .req(req),
		      .addr(pc[`ROM_ADDR_W-1:0]),
		      .data(data_rom)
		      );
   
   xinstr_mem instr_mem(
			.clk(clk),
			.pc(pc),
			.instruction(data_mem),
 			.dma_wnr(dma_wnr),
			.dma_addr(dma_addr),
			.dma_data(dma_data)
			);
   
endmodule
