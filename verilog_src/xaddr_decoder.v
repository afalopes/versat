/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>

***************************************************************************** */

`timescale 1ns / 1ps
`include "./xdefs.v"
`include "./xmem_map.v"

module xaddr_decoder(
	 input 		rw_req,
    input 		[`INT_ADDR_W-1:0] rw_addr,
    input               [`DATA_W-1:0] dma_data_to_rd,
    input               [`DATA_W-1:0] ctrl_data_to_rd,
    input               [`DATA_W-1:0] eng_data_to_rd,

    output reg conf_mem_req,	 
    output reg ctrl_regf_req,
    output reg eng_req,
    output reg dma_req,
    output reg [`DATA_W-1:0] data_to_rd
    );

	always @ (*) begin 
		ctrl_regf_req = 1'b0;
		conf_mem_req = 1'b0;
		eng_req = 1'b0;
                dma_req =1'b0;

		if (rw_addr < `DMA_BASE) begin
			ctrl_regf_req = rw_req;
                        data_to_rd = ctrl_data_to_rd;
                end else if (rw_addr < `PROG_MEM_BASE) begin
                        dma_req = rw_req;
                        data_to_rd = dma_data_to_rd;
		end else if (rw_addr < `ENG_BASE) begin
			conf_mem_req = rw_req;
		end else begin
			eng_req = rw_req; 
                        data_to_rd = eng_data_to_rd;
                end
	end 

endmodule
