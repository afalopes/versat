%{
// 
// Expression Interpreter, Parser Portion

/* This interpreter evaluates arithmetic expressions and assigns
   them to the specified variable names. The grammar is:

   pgm -> stmt*
   stmt -> id = exp
   exp -> exp + exp | exp - exp | exp * exp | exp / exp | ( exp ) | id | number
*/

#include <iostream>
#include <string>
#include <stdlib.h>
#include <map>

using namespace std;

// our hash table for variable names
  map<string, float> idTable;

// for keeping track of line numbers in the program we are parsing
  int line_num = 1;

// function prototypes, we need the yylex return prototype so C++ won't complain
int yylex();
void yyerror(const char * s);

%}

%start program

%union {
  float num;
  char *id;
}

%error-verbose

%token <num> NUMBER
%token <id> ID
%token NEWLINE
%token EQUALS PRINT

%left PLUS MINUS 
%left TIMES DIVIDE 
%token DPLUS
%token SEMIC 

%token LT

%left LPAREN RPAREN
%left LCURL RCURL
%left LSQR RSQR

%token FOR 

%nonassoc UMINUS
%type <num> exp 
%type <num> stmt

%%

program : forstmtlist
;

forstmtlist : forstmtlist NEWLINE    /* empty line */
         | forstmtlist forstmt NEWLINE
         |    /* empty string */
;

forstmt: FOR LPAREN ID EQUALS NUMBER SEMIC ID LT NUMBER SEMIC ID DPLUS RPAREN LCURL NEWLINE stmtlist RCURL {
  if($5 != 0)
     yyerror("Index initial value must be 0 in for statement\n");
  else if(string($3) != string($7))
    yyerror("Inconsistent index names in for statement\n");
  else if(string($7) != string($11))
    yyerror("Inconsistent index names in for statement\n");}
;

stmtlist : stmtlist NEWLINE    /* empty line */
         | stmtlist stmt NEWLINE/* new statement */ 
	 | stmtlist stmt  /* new statement */ 
         |    /* empty string */
;


stmt: ID EQUALS exp SEMIC { 
   idTable[$1] = $3;
   //cout << $3 << endl; 
   $$ = $3;
  }

 ;

exp:	MINUS exp %prec UMINUS {
  $$ = -$2; }

	|	exp PLUS exp {
				$$ = $1 + $3; }

	|	exp MINUS exp {
				$$ = $1 - $3; }

	|	exp TIMES exp {
				$$ = $1 * $3; }

	|	exp DIVIDE exp {
				$$ = $1 / $3; }

	|	LPAREN exp RPAREN  {
				 $$ = $2; }

	|	NUMBER {
				$$ = yylval.num; }

|       ID {
  $$ = idTable[$1]; }
;
 
%%
main()
{
  //  yydebug = 1;
  yyparse();
}

void yyerror(const char * s)
{
  fprintf(stderr, "line %d: %s\n", line_num, s);
}

