%{
/* * * * * * * * * * * *
 * * * DEFINITIONS * * *
 * * * * * * * * * * * */
%}

%{
// Ray Byler, CS560, 23 April 96
// Expression Interpreter, Lexer Portion

// y.tab.h contains the token number values produced by the parser
#include <string.h>
#include "y.tab.h"

extern int line_num;

%}

DIGIT [0-9]
LETTER [a-zA-Z]

%{ 
  /* * * * * * * * * *
   * * * STATES  * * *
   * * * * * * * * * */
%}

%x ERROR

%%

%{
/* * * * * * * * * 
 * * * RULES * * *
 * * * * * * * * */
%}

print   { return PRINT; }
for   { return FOR; }

{DIGIT}+ {
	yylval.num = atof(yytext); return NUMBER;
	}

{LETTER}[{DIGIT}{LETTER}]* {
        yylval.id = strdup(yytext); return ID;
	}

[ \t\f\r]	;		 // ignore white space 

\n      { line_num++; return NEWLINE; }
"-"	{ return MINUS;  }
"+"	{ return PLUS;   }
"++"	{ return DPLUS;   }
"*"	{ return TIMES;  }
"/"	{ return DIVIDE; }
"("	{ return LPAREN; }
")"	{ return RPAREN; }
"{"	{ return LCURL; }
"}"	{ return RCURL; }
"["	{ return LSQR; }
"]"	{ return RSQR; }
"="     { return EQUALS; }
";"     { return SEMIC; }
"<"     { return LT; }

. { BEGIN(ERROR); yymore(); }
&lt;ERROR&gt;[^{DIGIT}{LETTER}+\-/*()= \t\n\f\r] { yymore(); }
&lt;ERROR&gt;(.|\n) { yyless(yyleng-1); printf("error token: %s on line %d\n", yytext, line_num); 
           BEGIN(INITIAL); }

%%
